@component('mail::message')

<h1 style="color: #3498db;">Hello, {{ $user->name }}</h1>

<p  style="color: #000000;">Selamat datang di {{ config('app.name') }}! Untuk menyelesaikan pengaturan akun Anda, silakan menekan tombol verifikasi di bawah ini:</p>

@component('mail::button', ['url' => url('verify/' . $user->remember_token)])
Verifikasi
@endcomponent

<p  style="color: #000000;">Jika anda tidak mendaftar, tolong abaikan pesan ini</p>


Terima Kasih <br>
{{ config('app.name') }}
@endcomponent

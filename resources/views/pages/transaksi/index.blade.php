<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Proses Pembayaran</title>
    <!-- Tambahkan link CSS Tailwind di sini -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-v-0qO4vDIfRMFowU"></script>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;300;500;700;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Arimo:wght@400;500;600;700&display=swap" rel="stylesheet">

    <style>
        .font-league {
            font-family: 'League Spartan', sans-serif;
        }

        .custom-transition {
            transition: background-color 0.3s ease-in-out, transform 0.3s ease-in-out;
        }

        .custom-transition:hover {
            background-color: #000;
            transform: translateX(2px);
        }

        .animate-change {
            transition: transform 0.3s ease-in-out;
            display: inline-block;
        }

        .changed {
            transform: translateY(1em);
        }
    </style>
</head>

<body>
    @extends('pages.sidebar.index')

    @section('content')
        <div class="flex justify-center my-8 mx-40">
            <div class="flex justify-center items-center gap-60">
                <h1 class="w-[500px] text-8xl font-bold font-league">
                    Jelajahi Dunia Pembelajaran Bahasa dengan Fitur
                    <span id="premium" class="text-[#CC0A4D] animate-change">Premium</span>
                </h1>

                <div class="flex flex-col justify-end ">
                    <div class="my-20">
                        <del class="text-[30px] text-red-600 ">Rp200.000</del>
                        <p class="text-[60px] font-bold font-league text-black">
                            <span class="text-[30px]">Hanya dengan</span> Rp100.000
                        </p>
                        <p class="w-[500px] text-[28px] font-light font-league text-end">Anda dapat menjelajahi
                            semua
                            materi
                            dan
                            bahasa
                            tanpa adanya
                            batasan
                            !</p>
                    </div>
                    <button id="pay-button" class="bg-[#CC0A4D] text-white px-8 py-2 rounded-full my custom-transition">
                        Upgrade Akun
                        <span class="ml-2 inline-block">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                                class="h-4 w-4 inline-block">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7">
                                </path>
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            document.getElementById('pay-button').onclick = function(event) {
                event.preventDefault();

                // Add this line to get the CSRF token
                const csrfToken = document.querySelector('meta[name="csrf-token"]').content;

                // Make an Ajax request to create the transaction
                fetch('/transaksi', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': csrfToken,
                        },
                        body: JSON.stringify({
                            // Add any additional data you want to send to the server
                        }),
                    })
                    .then(response => response.json())
                    .then(data => {
                        if (data.snapToken) {
                            snap.pay(data.snapToken);
                        } else {
                            console.error('Error creating transaction:', data.error);
                        }
                    })
                    .catch(error => {
                        console.error('Error creating transaction:', error);
                    });
            };

            const premiumElement = document.getElementById('premium');
            const words = ['Premium', 'Golingua+'];

            setInterval(() => {
                premiumElement.classList.add('changed');
                setTimeout(() => {
                    premiumElement.textContent = words.reverse()[0];
                    premiumElement.classList.remove('changed');
                }, 300);
            }, 5000); // Change word every 3 seconds
        </script>
    @endsection
</body>

</html>

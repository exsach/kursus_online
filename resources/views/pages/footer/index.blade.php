<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Document</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;300;500;700;900&display=swap"
        rel="stylesheet">
</head>

<body>
    <div class="flex flex-col">
        <div class="flex gap-20 mx-24 justify-between my-20">
            <img src="/img/logo.png" alt="" class="w-[300px] h-full">
            <div class="flex flex-col gap-5">
                <h5 class="font-['Raleway'] font-bold text-[#4B4E53]">ABOUT US</h5>
                <div class="text-[#7E8082] font-['Raleway'] flex flex-col font-bold">
                    <a href="javascript:void(0);"  onclick="scrollToSection('pengertian')" class="hover:underline hover:text-[#494949]">PENGERTIAN</a>
                    <a href="javascript:void(0);"  onclick="scrollToSection('bahasa')" class="hover:underline hover:text-[#494949]">BAHASA</a>
                    <a href="javascript:void(0);" onclick="scrollToSection('fitur-fitur')" class="hover:underline hover:text-[#494949]">FITUR-FITUR</a>
                    <a href="/contact" class="hover:underline hover:text-[#494949]">CONTACT</a>
                </div>
            </div>
            <div class="flex flex-col gap-5">
                <h5 class="font-['Raleway'] font-bold text-[#4B4E53]">SOCIAL MEDIA</h5>
                <div class="text-[#7E8082] font-['Raleway'] flex flex-col">
                    <a href="https://instagram.com/golingua.ofc" target="_blank" class="hover:underline hover:text-[#494949]">INSTAGRAM</a>
                    <a href="" class="hover:underline hover:text-[#494949]">LINKEDIN</a>
                    <a href="" class="hover:underline hover:text-[#494949]">TWITTER</a>
                    <a href="" class="hover:underline hover:text-[#494949]">YOUTUBE</a>
                </div>
            </div>
            <div class="flex flex-col text-['Raleway'] text-[#7E8082] w-96 gap-2">
                <a href="https://www.google.com/maps/place/Jl.+Tentara+Genie+Pelajar,+Kec.+Sawahan,+Surabaya,+Jawa+Timur+60252/@-7.2584913,112.7230707,17z/data=!3m1!4b1!4m6!3m5!1s0x2dd7f94fe91f6d8f:0xcdf2d7701632ce9a!8m2!3d-7.2584966!4d112.7256456!16s%2Fg%2F11c594bwh2?entry=ttu" class="font-semibold" target="_blank">Tentara Genie Pelajar No.26, Petemon, Kec. Sawahan, Surabaya,
                    Jawa Timur 60252</a>
                <a href="tel:0315343708" class="hover:underline hover:text-[#494949]">0315343708</a>
                <a href="tel:089514581160" class="hover:underline hover:text-[#494949]">089514581160</a>
                <a href="mailto:golingua@gmail.com" class="hover:underline hover:text-[#494949]">golingua@gmail.com</a>
            </div>
        </div>
        <div class="flex justify-center my-10 text-[#7E8082]">
            <p>© GOLINGUA 2023</p>
        </div>
    </div>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sidebar - GoLingua</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;300;500;700;900&display=swap"
        rel="stylesheet">

    <style>
        .font-league {
            font-family: 'League Spartan', sans-serif;
        }
    </style>
</head>

<body>
    @php
        $kelas = request()->routeIs('course.index');
        $transaksi = request()->routeIs('transaksi');

        $transaksiUser = \App\Models\Transaksi::where('email', auth()->user()->email)
            ->where('status', 'sukses')
            ->first();
    @endphp

    <div>
        <nav class="fixed top-0 z-50 w-full bg-white border-b border-gray-200 ">
            <div class="px-3 py-6 lg:px-5 lg:pl-3">
                <div class="flex items-center justify-between">
                    <div class="flex items-center justify-start rtl:justify-end">
                        <button data-drawer-target="logo-sidebar" data-drawer-toggle="logo-sidebar"
                            aria-controls="logo-sidebar" type="button"
                            class="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg sm:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200">
                            <span class="sr-only">Open sidebar</span>
                            <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path clip-rule="evenodd" fill-rule="evenodd"
                                    d="M2 4.75A.75.75 0 012.75 4h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 4.75zm0 10.5a.75.75 0 01.75-.75h7.5a.75.75 0 010 1.5h-7.5a.75.75 0 01-.75-.75zM2 10a.75.75 0 01.75-.75h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 10z">
                                </path>
                            </svg>
                        </button>
                        <a href="/" class="flex ms-2 md:me-24">
                            <img src="{{ asset('img/logo.png') }}" class="h-12 me-3" alt="GoLingua Logo" />
                        </a>
                    </div>
                    <div class="flex items-center">
                        <div class="flex items-center ms-3 gap-5">
                            @if ($transaksiUser)
                                <svg width="25px" height="25px" viewBox="0 0 28 28" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M20.75 3C21.0557 3 21.3421 3.13962 21.5303 3.3746L21.6048 3.48102L25.8548 10.481C26.0556 10.8118 26.0459 11.2249 25.8395 11.5435L25.7634 11.6459L14.7634 24.6459C14.3906 25.0865 13.7317 25.1159 13.3207 24.7341L13.2366 24.6459L2.23662 11.6459C1.98663 11.3505 1.93182 10.941 2.08605 10.5941L2.14522 10.481L6.39522 3.48102C6.55388 3.21969 6.82182 3.04741 7.1204 3.00842L7.25001 3H20.75ZM17.515 12H10.484L13.999 20.672L17.515 12ZM22.844 12H19.673L16.756 19.195L22.844 12ZM8.326 12H5.155L11.242 19.193L8.326 12ZM9.674 5H7.81101L4.775 10H8.245L9.674 5ZM16.246 5H11.753L10.324 10H17.675L16.246 5ZM20.188 5H18.325L19.754 10H23.224L20.188 5Z"
                                        fill="#212121" />
                                </svg>
                            @endif
                            <div class="w-8 h-8 overflow-hidden rounded-full">
                                <a href="{{ route('user.editProfile') }}">
                                    @if (auth()->user()->profile_image)
                                        <img src="{{ asset('storage/profile_images/' . auth()->user()->profile_image) }}"
                                            class="object-cover w-full h-full">
                                    @else
                                        <div class="bg-red-100 w-full h-full"></div>
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <aside id="logo-sidebar"
            class="fixed top-0 left-0 z-40 w-64 h-screen pt-20 transition-transform -translate-x-full bg-white border-r border-gray-200 sm:translate-x-0"
            aria-label="Sidebar">
            <div class="h-full px-3 pb-4 overflow-y-auto bg-white py-10">
                <ul class="space-y-2 font-medium">
                    <li class="rounded-full hover:bg-[#CC0A4D] group {{ $kelas ? 'bg-[#CC0A4D]' : '' }}">
                        <a href="{{ route('course.index') }}"
                            class="flex items-center py-3 px-10 text-gray-900 hover:text-white {{ $kelas ? 'text-white' : 'text-gray-900' }}">
                            <span class=" font-league text-[20px] font-bold">Kelas</span>
                        </a>
                    </li>
                    <li class="rounded-full hover:bg-[#CC0A4D] group">
                        <a href="{{ route('user.editProfile') }}"
                            class="flex items-center py-3 px-10 text-gray-900 hover:text-white">
                            <span class="flex-1 whitespace-nowrap font-league text-[20px] font-bold">Setting</span>
                        </a>
                    </li>
                    @if (!$transaksiUser)
                        <li class="rounded-full hover:bg-[#CC0A4D] group {{ $transaksi ? 'bg-[#CC0A4D]' : '' }}">
                            <a href="{{ route('transaksi') }}"
                                class="flex items-center py-3 px-10 text-gray-900 hover:text-white {{ $transaksi ? 'text-white' : 'text-gray-900' }}">
                                <span
                                    class="flex-1 whitespace-nowrap font-league text-[20px] font-bold">Golingua+</span>
                            </a>
                        </li>
                    @endif
                    <li class="rounded-full group">
                        <form action="{{ route('logout') }}" method="POST" role="search">
                            @csrf
                            @method('DELETE')
                            <button type="submit"
                                class="py-3 px-10 text-gray-900 hover:text-white hover:bg-[#CC0A4D] rounded-full w-full text-start">
                                <span class=" whitespace-nowrap font-league text-[20px] font-bold">
                                    Logout
                                </span>
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
        </aside>

        <div class="p-4 sm:ml-64">
            <div class="p-4 mt-24">
                @yield('content')
            </div>
        </div>
    </div>

</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>GoLingua</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;300;500;700;900&display=swap"
        rel="stylesheet">
    <style>
        .custom-input {
            border: 2px solid transparent;
            border-image: linear-gradient(to right, black, #C59361);
            border-image-slice: 1;
            padding: 0.5rem;
            /* Sesuaikan dengan kebutuhan Anda */
            font-size: 14px;
            /* Sesuaikan dengan kebutuhan Anda */
        }

        .font-league {
            font-family: 'League Spartan', sans-serif;
        }
    </style>
</head>

<body>
    @extends('pages.navbar.index')

    @section('content')
        <div class="bg-white">

            {{-- button scrollbar  --}}
            <button onclick="scrollToTop()" id="scrollToTopBtn"
                class="hidden bg-red-500 font-bold px-4 py-4 rounded-full shadow-2xl shadow-black  
    fixed bottom-8 right-8 hover:animate-bounce duration-500">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffffff"
                    class="bi bi-arrow-up-short" viewBox="0 0 16 16">
                    <path fill-rule="evenodd"
                        d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5" />
                </svg>
            </button>


            {{-- about us  --}}
            <div id="pengertian" class="flex flex-col justify-center items-center mx-[600px] py-32">
                <p class="font-league text-[50px] text-[#273041] font-semibold border-b-2 border-[#CC0A4D] py-5">
                    golingua</p>
                <p class="text-center my-10 text-[16px] font-['Raleway'] text-light">Golingua adalah platform belajar bahasa
                    daring yang inovatif, dirancang khusus untuk para pembelajar di Indonesia. Golingua menawarkan pelajaran
                    menarik, latihan interaktif, dan umpan balik personal untuk mempercepat kemampuan berbahasa Anda. </p>
                <a href="/register" class="px-14 py-2 bg-[#CC0A4D] hover:bg-[#ad0942] rounded-full">
                    <div class="text-white text-base font-['Raleway']">Start</div>
                </a>
            </div>

            <img src="{{ asset('img/tampilan-awal.png') }}" alt="belajar" class="pb-20">

            {{-- fitur fitur --}}
            <div id="fitur-fitur" class="flex flex-col justify-center items-start mx-60 py-32 gap-10">
                <div class="flex flex-col">
                    <p class="text-[18px] font-['Raleway'] text-[#273041] font-normal">Fitur-fitur</p>
                    <p class="text-[36px] font-league text-[#273041] font-bold border-b-2 border-[#CC0A4D] pb-5">
                        golingua
                    </p>

                </div>
                <div class="grid grid-cols-4 gap-5">
                    <div class="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow-lg hover:bg-gray-100">

                        <h5 class="mb-2 text-[24px] font-bold tracking-tight text-gray-900 font-league">Pemula hingga Lanjut
                        </h5>
                        <p class="font-normal text-gray-400 text-[16px] font-['Raleway']">Materi cocok untuk pemula maupun
                            tingkat
                            lanjut.</p>
                    </div>
                    <div class="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow-lg hover:bg-gray-100">

                        <h5 class="mb-2 text-[24px] font-bold tracking-tight text-gray-900 font-league">Interaktif & Menarik
                        </h5>
                        <p class="font-normal text-gray-400 text-[16px] font-['Raleway']">Latihan, dan kuis, meningkatkan
                            daya tarik dan efektivitas belajar.</p>
                    </div>
                    <div class="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow-lg hover:bg-gray-100">

                        <h5 class="mb-2 text-[24px] font-bold tracking-tight text-gray-900 font-league">Dukungan Bahasa
                            Indonesia</h5>
                        <p class="font-normal text-gray-400 text-[16px] font-['Raleway']">Antarmuka dan materi pembelajaran
                            dalam Bahasa Indonesia, memudahkan pemahaman dan navigasi.</p>
                    </div>
                    <div class="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow-lg hover:bg-gray-100">

                        <h5 class="mb-2 text-[24px] font-bold tracking-tight text-gray-900 font-league">Pengajaran
                            Kontekstual</h5>
                        <p class="font-normal text-gray-400 text-[16px] font-['Raleway']">Pengajaran bahasa dalam konteks
                            sehari-hari yang membantu menerapkan pengetahuan dengan lebih efektif.</p>
                    </div>
                </div>
            </div>

            {{-- bahasa golingua  --}}
            <div id="bahasa" class="flex justify-between">
                <div class="flex flex-col ml-40">
                    <p class="text-[36px] font-league text-[#273041] font-bold leading-tight">Bahasa-bahasa apa yang dapat
                        dipelajari di platform GoLingua?</p>
                    <div class="border border-[#CC0A4D] w-20 my-10"></div>
                    @livewire('language-carousel', ['bahasas' => $bahasas])
                </div>
                <div>
                    <img src="{{ asset('img/world.png') }}" alt="" class="">
                </div>
            </div>

            <div class="bg-[#ffc2d7] justify-center items-center py-40 flex flex-col">
                <h1 class="font-league text-[35px] font-bold ">Apakah kamu siap belajar di <span
                        class="text-white bg-[#CC0A4D]">golingua</span>?</h1>
                <button class="bg-[#de4077] hover:bg-[#e33270] px-10 py-2 rounded-full">
                    <a href="{{ route('register') }}" class="font-league text-white text-[18px] font-bold">Siap</a>
                </button>
            </div>


        </div>
        @include('pages.footer.index')
    @endsection

    <script>
        // scrollbar start 
        function scrollToTop() {
            window.scrollTo({
                top: 0,
                behavior: 'smooth'
            });
        }

        document.addEventListener("DOMContentLoaded", function() {
            var scrollToTopBtn = document.getElementById('scrollToTopBtn');

            window.onscroll = function() {
                console.log('Scroll position:', window.pageYOffset);

                if (window.pageYOffset > window.innerHeight / 2 || window.pageYOffset > 0) {
                    console.log('Showing button');
                    scrollToTopBtn.classList.remove('hidden');
                } else {
                    console.log('Hiding button');
                    scrollToTopBtn.classList.add('hidden');
                }
            };
        });

        // scrollbar end 

        // footer scroll start
        function scrollToSection(sectionId) {
            var element = document.getElementById(sectionId);
            if (element) {
                element.scrollIntoView({
                    behavior: 'smooth'
                });
            }
        }
        // footer scroll end 

        // submit form start 
        async function submitForm(event) {
            event.preventDefault();

            try {
                const response = await fetch('{{ route('submit-report') }}', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    body: JSON.stringify({
                        first_name: document.getElementById('first_name').value,
                        last_name: document.getElementById('last_name').value,
                        email: document.getElementById('email').value,
                        phone_number: document.getElementById('phone_number').value,
                        message: document.getElementById('message').value,
                    }),
                });

                const data = await response.json();

                // Menanggapi hasil pengiriman formulir
                if (data.success) {
                    alert('Pesan Anda berhasil terkirim!');
                    // Reset form fields
                    document.getElementById('first_name').value = '';
                    document.getElementById('last_name').value = '';
                    document.getElementById('email').value = '';
                    document.getElementById('phone_number').value = '';
                    document.getElementById('message').value = '';
                } else {
                    alert('Gagal mengirim pesan. Silakan coba lagi.');
                }
            } catch (error) {
                console.error('Error:', error);
                alert('Terjadi kesalahan. Silakan coba lagi.');
            }
        }
        //submit form end 
    </script>

</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Navbar</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <style>
        body {
            margin: 0;
            font-family: 'Nunito', sans-serif;
            padding-top: 0px;
            /* Sesuaikan dengan tinggi navbar */
        }

        .bg-white-scroll {
        background-color: white !important;
        box-shadow: 0 4px 6px -6px gray;
    }
    </style>
</head>

<body>
    <nav class="border-gray-200 fixed top-0 w-full transition-colors duration-300 py-2">
        <div
            class="flex flex-wrap items-center justify-between mx-auto p-4 bg-transparent transition-colors duration-300">
            <a href="/" class="flex items-center space-x-3 rtl:space-x-reverse ml-10">
                <img src="{{ asset('img/logo.png') }}" class="h-10" />
            </a>
            <div class="hidden w-full md:block md:w-auto mr-10" id="navbar-default">
                <ul class="flex gap-5 justify-center items-center">
                    <li>
                        <a href="{{ route('home') }}"
                            class="font-light font-['Raleway'] text-[#000] text-[16px] hover:text-gray-900">Home</a>
                    </li>
                    {{-- <li>
                        <a href="#"
                            class="font-light font-['Raleway'] text-[#000] text-[16px] hover:text-gray-900">Fitur</a>
                    </li> --}}
                    <li>
                        <a href="{{ route('report') }}"
                            class="font-light font-['Raleway'] text-[#000] text-[16px] hover:text-gray-900">Contact</a>
                    </li>
                    <li>
                        @if (Route::has('login'))
                            @auth

                                <form action="{{ route('logout') }}" method="POST" role="search">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit"
                                        class="px-5 py-2 bg-[#CC0A4D] hover:bg-[#ad0942] rounded-full text-white">Logout</button>
                                </form>
                            @else
                                <a href="{{ route('login') }}"
                                    class="px-5 py-2 border-2 border-[#CC0A4D] hover:bg-[#CC0A4D] rounded-full text-[#CC0A4D] hover:text-white">LOG
                                    IN</a>

                        <li>
                            @if (Route::has('register'))
                                <a href="{{ route('register') }}"
                                    class="px-5 py-2 bg-[#CC0A4D] hover:bg-[#ad0942] rounded-full text-white">REGISTER</a>
                            @endif

                        </li>
                    @endauth
                    @endif
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <main>
        @yield('content')
    </main>

    <script>
        window.addEventListener('scroll', scrollFunction);
    
        function scrollFunction() {
            var navbar = document.querySelector("nav");
    
            if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                navbar.classList.add("bg-white-scroll");
            } else {
                navbar.classList.remove("bg-white-scroll");
            }
        }
    </script>
</body>

</html>

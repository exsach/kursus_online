<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>GoLingua</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;300;500;700;900&display=swap"
        rel="stylesheet">
    <style>
        .custom-input {
            border: 2px solid transparent;
            border-image: linear-gradient(to right, black, #C59361);
            border-image-slice: 1;
            padding: 0.5rem;
            /* Sesuaikan dengan kebutuhan Anda */
            font-size: 14px;
            /* Sesuaikan dengan kebutuhan Anda */
        }

        .font-league {
            font-family: 'League Spartan', sans-serif;
        }
    </style>
</head>

<body>
    @extends('pages.navbar.index')

    @section('content')
        <section class="bg-[#cc0a4e26] py-10" id="contact">
            <div class="mx-auto max-w-7xl px-4 py-16 sm:px-6 lg:px-8 lg:py-20">
                <div class="mb-4">
                    <div class="mb-6 max-w-3xl text-center sm:text-center md:mx-auto md:mb-12">
                        <h2
                            class="font-heading mb-4 font-bold tracking-tight text-gray-900 text-3xl sm:text-5xl font-league">
                            Get in Touch
                        </h2>
                    </div>
                </div>
                <div class="flex items-stretch justify-center bg-white rounded-xl">
                    <div class="grid md:grid-cols-2">
                        <div class="bg-[#CC0A4D] m-3 p-10 rounded-xl">
                            <p class="mt-3 mb-12 text-lg text-[#eaeaea] font-['Raleway']">
                                Class aptent taciti sociosqu ad
                                litora torquent per conubia nostra, per inceptos himenaeos. Duis nec ipsum orci. Ut
                                scelerisque
                                sagittis ante, ac tincidunt sem venenatis ut.
                            </p>
                            <ul class="mb-6 md:mb-0 ">
                                <li class="flex">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" class="h-6 w-6">
                                        <path d="M9 11a3 3 0 1 0 6 0a3 3 0 0 0 -6 0" stroke="white"></path>
                                        <path
                                            d="M17.657 16.657l-4.243 4.243a2 2 0 0 1 -2.827 0l-4.244 -4.243a8 8 0 1 1 11.314 0z"
                                            stroke="white"></path>
                                    </svg>

                                    <div class="ml-4 mb-4">
                                        <h3 class="mb-2 text-lg font-bold leading-6 text-white font-league">Our Address
                                        </h3>
                                        <a href="">
                                        <p class="text-[#ffe9f0] font-['Raleway']">Tentara Genie Pelajar No.26, Petemon, Kec. Sawahan,</p>
                                        <p class="text-[#ffe9f0] font-['Raleway']">Surabaya, Jawa Timur 60252</p>
                                    </a>
                                    </div>
                                </li>
                                <li class="flex">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" class="h-6 w-6">
                                        <path
                                            d="M5 4h4l2 5l-2.5 1.5a11 11 0 0 0 5 5l1.5 -2.5l5 2v4a2 2 0 0 1 -2 2a16 16 0 0 1 -15 -15a2 2 0 0 1 2 -2"
                                            stroke="white"></path>
                                        <path d="M15 7a2 2 0 0 1 2 2" stroke="white"></path>
                                        <path d="M15 3a6 6 0 0 1 6 6" stroke="white"></path>
                                    </svg>

                                    <div class="ml-4 mb-4">
                                        <h3 class="mb-2 text-lg font-bold leading-6 text-white font-league">Contact
                                        </h3>
                                        <p class="text-[#ffe9f0] font-['Raleway']">Mobile: 0315343708</p>
                                        <p class="text-[#ffe9f0] font-['Raleway']">Mail: golingua@gmail.com</p>
                                    </div>
                                </li>
                                <li class="flex">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" class="h-6 w-6">
                                        <path d="M3 12a9 9 0 1 0 18 0a9 9 0 0 0 -18 0"></path>
                                        <path d="M12 7v5l3 3"></path>
                                    </svg>

                                    <div class="ml-4 mb-4">
                                        <h3 class="mb-2 text-lg font-bold leading-6 text-white font-league">Working
                                            hours</h3>
                                        <p class="text-[#ffe9f0] font-['Raleway']">Monday - Friday: 08:00 - 17:00</p>
                                        <p class="text-[#ffe9f0] font-['Raleway']">Saturday &amp; Sunday: 08:00 - 12:00</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="px-10 py-10 flex justify-center items-center" id="form">
                            <form id="reportForm" onsubmit="submitForm(event)" action="{{ route('submit-report') }}"
                                method="post" class="w-full flex flex-col gap-5">
                                @csrf

                                @if (session('success'))
                                    <div class="bg-green-500 text-white p-4 mb-4">
                                        {{ session('success') }}
                                    </div>
                                @endif

                                <div class="flex gap-5">
                                    <div class="form-field border-b border-gray-300 focus-within:border-[#CC0A4D]">
                                        <label for="first_name" class="text-sm font-['Raleway']">First name</label>
                                        <input type="text" id="first_name" name="first_name"
                                            class="text-gray-900 sm:text-md outline-none block w-full font-['Poppins'] font-semibold py-1 px-1"
                                            required>
                                    </div>
                                    <div class="form-field border-b border-gray-300 focus-within:border-[#CC0A4D]">
                                        <label for="last_name" class="text-sm font-['Raleway']">Last name</label>
                                        <input type="text" id="last_name" name="last_name"
                                            class="text-gray-900 sm:text-md outline-none block w-full font-['Poppins'] font-semibold py-1 px-1"
                                            required>
                                    </div>
                                </div>
                                <div class="form-field border-b border-gray-300 focus-within:border-[#CC0A4D]">
                                    <label for="email" class="text-sm font-['Raleway']">Email</label>
                                    <input type="email" id="email" name="email"
                                        class="text-gray-900 sm:text-md outline-none block w-full font-['Poppins'] font-semibold py-1 px-1"
                                        required>
                                </div>
                                <div class="form-field border-b border-gray-300 focus-within:border-[#CC0A4D]">
                                    <label for="phone_number" class="text-sm font-['Raleway']">Phone number</label>
                                    <input type="text" id="phone_number" name="phone_number"
                                        class="text-gray-900 sm:text-md outline-none block w-full font-['Poppins'] font-semibold py-1 px-1"
                                        required>
                                </div>
                                <div class="form-field border-b border-gray-300 focus-within:border-[#CC0A4D]">
                                    <label for="message" class="text-sm font-['Raleway']">Message</label>
                                    <textarea id="message" name="message"
                                        class="text-gray-900 sm:text-md outline-none block w-full font-['Poppins'] font-semibold py-1 px-1" required></textarea>
                                </div>
                                <div>
                                    <button type="submit" onclick="submitForm()"
                                        class="text-white bg-[#CC0A4D] hover:bg-[#a3073d] focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-full text-sm px-10 py-2.5 text-center font-['Raleway']">
                                        Send Message
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @include('pages.footer.index')
    @endsection

    <script>
        async function submitForm(event) {
            event.preventDefault();

            try {
                const response = await fetch('{{ route('submit-report') }}', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    body: JSON.stringify({
                        first_name: document.getElementById('first_name').value,
                        last_name: document.getElementById('last_name').value,
                        email: document.getElementById('email').value,
                        phone_number: document.getElementById('phone_number').value,
                        message: document.getElementById('message').value,
                    }),
                });

                const data = await response.json();

                // Menanggapi hasil pengiriman formulir
                if (data.success) {
                    alert('Pesan Anda berhasil terkirim!');
                    // Reset form fields
                    document.getElementById('first_name').value = '';
                    document.getElementById('last_name').value = '';
                    document.getElementById('email').value = '';
                    document.getElementById('phone_number').value = '';
                    document.getElementById('message').value = '';
                } else {
                    alert('Gagal mengirim pesan. Silakan coba lagi.');
                }
            } catch (error) {
                console.error('Error:', error);
                alert('Terjadi kesalahan. Silakan coba lagi.');
            }
        }
    </script>
</body>

</html>

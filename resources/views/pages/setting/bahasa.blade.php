<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Setting - Akun</title>

    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;300;500;700;900&display=swap"
        rel="stylesheet">

    <style>
        .font-league {
            font-family: 'League Spartan', sans-serif;
        }
    </style>
</head>

<body>
    @extends('pages.setting.sidebar')

    @section('content')

        @php
            $userHasSuccessfulTransaction = \App\Models\Transaksi::where('email', $user->email)
                ->where('status', 'sukses')
                ->exists();
        @endphp

        <div class="flex flex-col justify-between mx-60 py-20 gap-20">
            <a href="{{ route('course.index') }}" class="hover:text-slate-500">
                <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </a>
            @if ($userHasSuccessfulTransaction)
                <form action="{{ route('language.update') }}" method="POST" enctype="multipart/form-data"
                    class="space-y-4 md:space-y-6">
                    @csrf
                    @method('PUT')

                    <div class="border-2 border-[#CC0A4D] py-2 px-2 rounded-xl">
                        <select name="language_id" id="language_id"
                            class="text-gray-900 sm:text-sm outline-none block w-full p-2.5">
                            @foreach ($bahasas as $bahasa)
                                <option value="{{ $bahasa->id }}" class="text-gray-900"
                                    @if ($user->language_id == $bahasa->id) selected @endif>
                                    {{ $bahasa->bahasa }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit"
                        class=" text-white bg-[#CC0A4D] hover:bg-[#a3073d] focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-full text-sm px-10 py-2.5 text-center">Update
                        Bahasa</button>
                </form>
            @else
                <form action="{{ route('transaksi') }}" method="GET" enctype="multipart/form-data"
                    class="space-y-4 md:space-y-6">
                    @csrf
                    @method('PUT')

                    <div class="border-2 border-[#CC0A4D] py-2 px-2 rounded-xl">
                        <select name="language_id" id="language_id"
                            class="text-gray-900 sm:text-sm outline-none block w-full p-2.5">
                            @foreach ($bahasas as $bahasa)
                                <option value="{{ $bahasa->id }}" class="text-gray-900"
                                    @if ($user->language_id == $bahasa->id) selected @endif>
                                    {{ $bahasa->bahasa }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit"
                        class=" text-white bg-[#CC0A4D] hover:bg-[#a3073d] focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-full text-sm px-10 py-2.5 text-center">Update
                        Bahasa</button>
                </form>
            @endif
        </div>
    @endsection
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Setting - Akun</title>

    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;300;500;700;900&display=swap"
        rel="stylesheet">

    <style>
        .font-league {
            font-family: 'League Spartan', sans-serif;
        }
    </style>
</head>

<body>
    @extends('pages.setting.sidebar')

    @section('content')
    <div class="flex flex-col justify-between mx-60 py-20 gap-20">
        <a href="{{ route('course.index') }}" class="hover:text-slate-500">
            <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </a>
        <form action="{{ route('user.updateProfile', ['id' => $user->id]) }}" method="POST"
            enctype="multipart/form-data" class="space-y-4 md:space-y-6">
            @csrf
            @method('PUT')

            <div class="flex items-center gap-20">
                <div class="mb-4 w-[300px] h-[300px] overflow-hidden rounded-full border-4 border-[#CC0A4D]">
                    <img src="{{ asset('storage/profile_images/' . $user->profile_image) }}"
                        class="object-cover w-full h-full">
                </div>
                <div class="mb-4">
                    <label for="profile_image" class="block text-gray-700 text-md font-bold mb-2 font-league">Upload New Profile
                        Image:</label>
                    <input type="file" name="profile_image" class="text-gray-700">
                </div>
            </div>
            <div class="border-2 border-[#CC0A4D] py-2 px-2 rounded-xl">
                <input type="text" name="name" value="{{ old('name', $user->name) }}"
                    class="text-gray-900 sm:text-sm outline-none block w-full p-2.5" placeholder="Nama"
                    required>
            </div>

            <div class="border-2 border-[#CC0A4D] py-2 px-2 rounded-xl">
                <input type="email" name="email" value="{{ old('email', $user->email) }}"
                    class="text-gray-900 sm:text-sm outline-none block w-full p-2.5" placeholder="Email"
                    required>
            </div>
            <button type="submit"
                class=" text-white bg-[#CC0A4D] hover:bg-[#a3073d] focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-full text-sm px-10 py-2.5 text-center">Update
                Profile</button>
        </form>
    </div>
    @endsection
</body>

</html>

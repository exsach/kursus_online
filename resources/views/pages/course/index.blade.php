<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Course - GoLingua</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
</head>

<body class="bg-white">
    @extends('pages.sidebar.index')

    @section('content')

        <form action="{{ route('course.index') }}" method="GET">
            <input class="w-full h-16 px-3 rounded mb-8 focus:outline-none focus:shadow-outline text-xl px-8 shadow-lg"
                type="search" name="search" placeholder="Search..." value="{{ request('search') }}">
            <button type="submit">Search</button>
        </form>

        @if ($materis->count() > 0)
            <div class="grid grid-cols-3 gap-10">
                @foreach ($materis as $index => $materi)
                    <div
                        class="bg-[#fff] shadow-xl shadow-[#cc0a4e30] flex justify-center gap-5 items-center border rounded-xl p-5">
                        <div class="w-52 h-52 overflow-hidden rounded-xl mr-2 my-2">
                            <img src="{{ asset('/storage/materis/' . $materi->image) }}" alt="Materi Image"
                                class="w-full h-full object-cover">
                        </div>
                        <div class="flex flex-col">
                            {{-- <h3 class="font-league font-bold text-2xl text-[#2b2b2b]"> Kelas {{ $index + 1 }} :</h3> --}}
                            <p class="font-league font-bold text-2xl text-[#2b2b2b]">{{ $materi->judul }}</p>
                            <a href="{{ route('materi.levels', ['id' => $materi->id]) }}" type="button"
                                class="text-white bg-[#CC0A4D] hover:bg-[#a3073d] focus:ring-4 focus:outline-none focus:ring-primary-300  rounded-full text-sm px-10 py-2.5 text-center mt-10 font-semibold">LANJUT</a>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="bg-white w-full flex flex-col items-center justify-center text-center">
                <div class="text-black ">
                    <h1 class="text-4xl font-bold">Coming Soon</h1>
                    <p class="mt-4 text-lg">Kami sedang mengerjakan sesuatu yang luar biasa!</p>
                </div>
                <div class="mt-8">
                    <p class="mt-2 text-gray-400 text-sm">Jadilah yang pertama mengetahui saat kami meluncurkan!</p>
                </div>
            </div>
        @endif
    @endsection

</body>

</html>

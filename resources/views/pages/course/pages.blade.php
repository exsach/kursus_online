<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kelas - GoLingua</title>

    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <style>
        .transition-effect {
            opacity: 0;
            transform: translateY(20px);
            transition: opacity 0.5s ease-in-out, transform 0.5s ease-in-out;
        }

        .transition-effect.active {
            opacity: 1;
            transform: translateY(0);
        }
    </style>
</head>

<body>
    <div class="flex flex-col justify-between h-screen mx-60 py-20">
        <a href="{{ route('materi.levels', ['id' => $materiId]) }}" class="hover:text-slate-500">
            <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </a>
        <div class="flex flex-col justify-center items-center h-screen">
            @forelse ($penjelasans as $penjelasan)
                <div class="flex justify-center items-center">
                    <div class="">
                        <ul>
                            <li class="flex flex-col gap-10 mx-40 -my-16 items-center justify-center transition-effect">
                                <h1 class="font-['Poppins'] font-bold text-[#151515] text-[40px]">
                                    {{ $penjelasan->judul }}
                                </h1>
                                <div class="flex items-start gap-20">
                                    @if ($penjelasan->image)
                                        <img src="{{ asset('/storage/penjelasans/' . $penjelasan->image) }}"
                                            class="w-40 transition-effect">
                                    @endif
                                    <div class="flex flex-col">
                                        <p
                                            class="font-['Poppins'] font-semibold text-[#5b5b5b] text-[25px] transition-effect">
                                            {{ $penjelasan->isi }}
                                        </p>
                                        <p
                                            class="font-['Poppins'] font-semibold text-[#5b5b5b] text-[25px] transition-effect">
                                            {!! nl2br(e($penjelasan->contoh)) !!}
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            @empty
                <div class="bg-white w-full h-full flex flex-col items-center justify-center text-center">
                    <div class="text-black ">
                        <h1 class="text-4xl font-bold">Coming Soon</h1>
                        <p class="mt-4 text-lg">Kami sedang mengerjakan sesuatu yang luar biasa!</p>
                    </div>
                    <div class="mt-8">
                        <p class="mt-2 text-gray-400 text-sm">Jadilah yang pertama mengetahui saat kami meluncurkan!</p>
                    </div>
                </div>
            @endforelse
        </div>
        <div class="flex justify-between items-center mx-10 w-full">
            <div>
                @if ($penjelasans->currentPage() > 1)
                    <a class="border border-[#CC0A4D] text-[#CC0A4D] block rounded-sm font-bold py-4 px-6 mr-2 flex items-center hover:bg-[#CC0A4D] hover:text-white"
                        href="{{ $penjelasans->previousPageUrl() }}">
                        <svg class="h-5 w-5 mr-2 fill-current" version="1.1" id="Layer_1"
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                            y="0px" viewBox="-49 141 512 512" style="enable-background:new -49 141 512 512;"
                            xml:space="preserve">
                            <path id="XMLID_10_"
                                d="M438,372H36.355l72.822-72.822c9.763-9.763,9.763-25.592,0-35.355c-9.763-9.764-25.593-9.762-35.355,0 l-115.5,115.5C-46.366,384.01-49,390.369-49,397s2.634,12.989,7.322,17.678l115.5,115.5c9.763,9.762,25.593,9.763,35.355,0 c9.763-9.763,9.763-25.592,0-35.355L36.355,422H438c13.808,0,25-11.193,25-25S451.808,372,438,372z">
                            </path>
                        </svg>
                    </a>
                @endif
            </div>

            <div>
                @if ($penjelasans->hasMorePages())
                    <a class="border border-[#CC0A4D] bg-[#CC0A4D] hover:bg-[#e33974] text-white block rounded-sm font-bold py-4 px-6 ml-2 flex items-center"
                        href="{{ $penjelasans->nextPageUrl() }}">
                        Lanjut
                        <svg class="h-5 w-5 ml-2 fill-current" clasversion="1.1" id="Layer_1"
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                            y="0px" viewBox="-49 141 512 512" style="enable-background:new -49 141 512 512;"
                            xml:space="preserve">
                            <path id="XMLID_11_"
                                d="M-24,422h401.645l-72.822,72.822c-9.763,9.763-9.763,25.592,0,35.355c9.763,9.764,25.593,9.762,35.355,0
                                l115.5-115.5C460.366,409.989,463,403.63,463,397s-2.634-12.989-7.322-17.678l-115.5-115.5c-9.763-9.762-25.593-9.763-35.355,0
                                c-9.763,9.763-9.763,25.592,0,35.355l72.822,72.822H-24c-13.808,0-25,11.193-25,25S-37.808,422-24,422z" />
                        </svg>
                    </a>
                @else
                    <a class="border border-[#CC0A4D] bg-[#CC0A4D] hover:bg-[#e33974] text-white block rounded-sm font-bold py-4 px-6 ml-2 flex items-center"
                        href="{{ route('quiz.pages', [$materiId, $levelId]) }}">
                        Lanjut
                        <svg class="h-5 w-5 ml-2 fill-current" clasversion="1.1" id="Layer_1"
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                            y="0px" viewBox="-49 141 512 512" style="enable-background:new -49 141 512 512;"
                            xml:space="preserve">
                            <path id="XMLID_11_"
                                d="M-24,422h401.645l-72.822,72.822c-9.763,9.763-9.763,25.592,0,35.355c9.763,9.764,25.593,9.762,35.355,0
                                l115.5-115.5C460.366,409.989,463,403.63,463,397s-2.634-12.989-7.322-17.678l-115.5-115.5c-9.763-9.762-25.593-9.763-35.355,0
                                c-9.763,9.763-9.763,25.592,0,35.355l72.822,72.822H-24c-13.808,0-25,11.193-25,25S-37.808,422-24,422z" />
                        </svg>
                    </a>
                @endif
            </div>
        </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".transition-effect").addClass("active");
        });
    </script>

</body>

</html>

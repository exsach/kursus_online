<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Select Language - GoLingua</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
        rel="stylesheet">

    <style>
        .language_id {
            position: absolute;
            opacity: 0;
        }

        .language-selector {
            position: relative;
        }

        .language-selector label {
            cursor: pointer;
            display: block;
        }
    </style>
</head>

<body class="bg-[#ffe7f0] flex justify-center items-center">
    <div class="flex justify-center items-center my-5 mx-40 h-full">
        <form action="{{ route('save.language') }}" method="post" id="languageForm"
            class="p-8 rounded grid grid-cols-6 gap-20">
            @csrf
            @foreach ($languages as $language)
                <div class="flex justify-center items-center language-selector">
                    <input type="radio" name="language_id" class="language_id" id="language_id_{{ $language->id }}"
                        value="{{ $language->id }}">
                    <label for="language_id_{{ $language->id }}" class="cursor-pointer text-center"
                        onclick="submitForm('{{ $language->id }}')">
                        <img src="{{ asset('/storage/bahasas/' . $language->image) }}" alt="{{ $language->bahasa }}"
                            class="shadow shadow-[#CC0A4D] w-40  rounded-full object-cover">
                        <p class="mt-5 font-['Open_Sans'] font-bold text-lg text-[#CC0A4D] ">{{ $language->bahasa }}</p>
                    </label>
                </div>
            @endforeach
        </form>
    </div>

    <script>
        function submitForm(languageId) {
            document.getElementById('language_id_' + languageId).checked = true;
            document.getElementById('languageForm').submit();
        }
    </script>
</body>


</html>

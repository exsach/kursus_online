<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Level - GoLingua</title>

    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;300;500;700;900&display=swap"
        rel="stylesheet">

    <style>
        .font-league {
            font-family: 'League Spartan', sans-serif;
        }
    </style>
</head>

<body>
    @extends('pages.sidebar.level')

    @section('content')
        <div class="flex flex-col">
            <div class="flex justify-center items-center">
                <p class="font-league text-[50px] font-bold text-[#CC0A4D]">Pilih Level</p>
            </div>
            @if ($levels->count() > 0)
                <div class="flex flex-wrap m-10 items-center">
                {{-- <div class="grid grid-cols-10 gap-60 m-20"> --}}
                    @foreach ($levels as $level)
                    <div class="m-10">
                        <div class="ring-2 ring-[#CC0A4D] rounded-full inline-block w-32 h-32 overflow-hidden p-2">
                            <a href="{{ route('level.pages', ['materiId' => $materi->id, 'levelId' => $level->id]) }}"
                                class="block w-full h-full rounded-full ">
                                <div class="bg-[#ff6fa2] w-full h-full rounded-full flex justify-center items-center">
                                    <span class="text-xl font-bold font-['Poppins']">
                                        {{ $level->nomor_level }}
                                    </span>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            @else
                <p>Level belum ada</p>
            @endif
        </div>
    @endsection
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quiz</title>

    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <style>
        .correct-answer {
            animation: explode 0.5s ease-in-out;
            color: green;
            /* Warna teks hijau untuk jawaban benar */
            border-color: green;
            /* Warna border untuk jawaban benar */
        }

        .wrong-answer {
            animation: cross 0.5s ease-in-out;
            color: red;
            /* Warna teks merah untuk jawaban salah */
            border-color: red;
            /* Warna border untuk jawaban salah */
        }

        @keyframes explode {
            0% {
                transform: scale(1);
            }

            50% {
                transform: scale(1.2);
            }

            100% {
                transform: scale(1);
            }
        }

        @keyframes cross {
            0% {
                transform: scale(1);
            }

            50% {
                transform: scale(1.2) rotate(45deg);
            }

            100% {
                transform: scale(1);
            }
        }

        .error-icon {
            font-size: 1.5em;
            /* Sesuaikan ukuran ikon sesuai keinginan Anda */
            margin-right: 8px;
            /* Berikan jarak kanan agar tidak rapat dengan teks */
        }

        /* Tambahkan aturan untuk border */
        button[data-question-id] {
            border-width: 2px;
            border-style: solid;
            transition: border-color 0.3s;
            /* Animasi perubahan warna border */
        }

        .transition-effect {
            opacity: 0;
            transform: translateY(20px);
            transition: opacity 0.5s ease-in-out, transform 0.5s ease-in-out;
        }

        .transition-effect.active {
            opacity: 1;
            transform: translateY(0);
        }

        /* ... */
    </style>



</head>

<body>
    <div class="flex flex-col justify-between h-screen mx-60 py-20">
        <a href="{{ route('materi.levels', ['id' => $materiId]) }}" class="hover:text-slate-500">
            <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </a>
        <div class="flex flex-col justify-center items-center h-screen">
            <div class="flex justify-center items-center">
                @forelse ($quizz as $quiz)
                    <div class="mb-10">
                        <ul>
                            <div class="flex flex-col gap-10 mx-40 my-32 items-center justify-center transition-effect">
                                @if ($quiz->image)
                                    <img src="{{ asset('/storage/quiz/' . $quiz->image) }}"
                                        class="w-40 transition-effect">
                                @endif
                                <div class="flex flex-col gap-20">
                                    <span id="error-message-{{ $quiz->id }}" class="transition-effect"></span>
                                    <h1 class="font-['Poppins'] font-bold text-[#151515] text-[40px] transition-effect">
                                        {{ $quiz->teks_soal }}
                                    </h1>
                                    <ul class="flex gap-20">
                                        <button data-question-id="{{ $quiz->id }}"
                                            class="border-2 border-[#909090] rounded-md p-10"
                                            onclick="checkAnswer(this, '{{ $quiz->opsi_a }}', '{{ $quiz->jawaban }}', 'A', '{{ $quiz->id }}')">
                                            {{ $quiz->opsi_a }}
                                        </button>
                                        <button data-question-id="{{ $quiz->id }}"
                                            class="border-2 border-[#909090] rounded-md p-10"
                                            onclick="checkAnswer(this, '{{ $quiz->opsi_b }}', '{{ $quiz->jawaban }}', 'B', '{{ $quiz->id }}')">
                                            {{ $quiz->opsi_b }}
                                        </button>
                                        <button data-question-id="{{ $quiz->id }}"
                                            class="border-2 border-[#909090] rounded-md p-10"
                                            onclick="checkAnswer(this, '{{ $quiz->opsi_c }}', '{{ $quiz->jawaban }}', 'C', '{{ $quiz->id }}')">
                                            {{ $quiz->opsi_c }}
                                        </button>
                                        <button data-question-id="{{ $quiz->id }}"
                                            class="border-2 border-[#909090] rounded-md p-10"
                                            onclick="checkAnswer(this, '{{ $quiz->opsi_d }}', '{{ $quiz->jawaban }}', 'D', '{{ $quiz->id }}')">
                                            {{ $quiz->opsi_d }}
                                        </button>
                                    </ul>
                                </div>
                            </div>
                        </ul>
                    </div>
                @empty
                    <p>SOON</p>
                @endforelse
            </div>
        </div>
        <div class="flex justify-between items-center mx-10 w-full">
            <div>
                @if ($penjelasans->currentPage() > 1)
                    <a href="{{ $penjelasans->previousPageUrl() }}"
                        class="border border-teal-500 text-teal-500 rounded-sm font-bold py-4 px-6 mr-2 flex items-center hover:bg-teal-500 hover:text-white">
                        <svg class="h-5 w-5 mr-2 fill-current" version="1.1" id="Layer_1"
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                            y="0px" viewBox="-49 141 512 512" style="enable-background:new -49 141 512 512;"
                            xml:space="preserve">
                            <path id="XMLID_10_"
                                d="M438,372H36.355l72.822-72.822c9.763-9.763,9.763-25.592,0-35.355c-9.763-9.764-25.593-9.762-35.355,0 l-115.5,115.5C-46.366,384.01-49,390.369-49,397s2.634,12.989,7.322,17.678l115.5,115.5c9.763,9.762,25.593,9.763,35.355,0 c9.763-9.763,9.763-25.592,0-35.355L36.355,422H438c13.808,0,25-11.193,25-25S451.808,372,438,372z">
                            </path>
                        </svg>

                    </a>
                @else
                    <a href="{{ route('level.pages', ['materiId' => $materiId, 'levelId' => $levelId, 'page' => $penjelasans->lastPage()]) }}"
                        class="border border-[#CC0A4D] text-[#CC0A4D] rounded-sm font-bold py-4 px-6 mr-2 flex items-center hover:bg-[#CC0A4D] hover:text-white"
                        disabled>
                        <svg class="h-5 w-5 mr-2 fill-current" version="1.1" id="Layer_1"
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                            y="0px" viewBox="-49 141 512 512" style="enable-background:new -49 141 512 512;"
                            xml:space="preserve">
                            <path id="XMLID_10_"
                                d="M438,372H36.355l72.822-72.822c9.763-9.763,9.763-25.592,0-35.355c-9.763-9.764-25.593-9.762-35.355,0 l-115.5,115.5C-46.366,384.01-49,390.369-49,397s2.634,12.989,7.322,17.678l115.5,115.5c9.763,9.762,25.593,9.763,35.355,0 c9.763-9.763,9.763-25.592,0-35.355L36.355,422H438c13.808,0,25-11.193,25-25S451.808,372,438,372z">
                            </path>
                        </svg>

                    </a>
                @endif
            </div>

            {{-- Pagination --}}
            <div>
                @if ($quizz->hasMorePages())
                    <a href="{{ $quizz->nextPageUrl() }}"
                        class="border border-[#CC0A4D] bg-[#CC0A4D] hover:bg-[#e33974] text-white rounded-sm font-bold py-4 px-6 ml-2 flex items-center"
                        id="nextButton" onclick="return checkSelectedAnswer()">
                        Lanjut
                        <svg class="h-5 w-5 ml-2 fill-current" clasversion="1.1" id="Layer_1"
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                            y="0px" viewBox="-49 141 512 512" style="enable-background:new -49 141 512 512;"
                            xml:space="preserve">
                            <path id="XMLID_11_"
                                d="M-24,422h401.645l-72.822,72.822c-9.763,9.763-9.763,25.592,0,35.355c9.763,9.764,25.593,9.762,35.355,0 l115.5-115.5C460.366,409.989,463,403.63,463,397s-2.634-12.989-7.322-17.678l-115.5-115.5c-9.763-9.762-25.593-9.763-35.355,0 c-9.763,9.763-9.763,25.592,0,35.355l72.822,72.822H-24c-13.808,0-25,11.193-25,25S-37.808,422-24,422z">
                            </path>
                        </svg>
                    </a>
                @else
                    <a href="{{ route('materi.levels', ['id' => $materiId]) }}"
                        class="border border-[#CC0A4D] bg-[#CC0A4D] hover:bg-[#e33974] text-white rounded-sm font-bold py-4 px-6 ml-2 flex items-center"
                        id="nextButton" onclick="return checkSelectedAnswer()" disabled>
                        Lanjut
                        <svg class="h-5 w-5 ml-2 fill-current" clasversion="1.1" id="Layer_1"
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                            y="0px" viewBox="-49 141 512 512" style="enable-background:new -49 141 512 512;"
                            xml:space="preserve">
                            <path id="XMLID_11_"
                                d="M-24,422h401.645l-72.822,72.822c-9.763,9.763-9.763,25.592,0,35.355c9.763,9.764,25.593,9.762,35.355,0 l115.5-115.5C460.366,409.989,463,403.63,463,397s-2.634-12.989-7.322-17.678l-115.5-115.5c-9.763-9.762-25.593-9.763-35.355,0 c-9.763,9.763-9.763,25.592,0,35.355l72.822,72.822H-24c-13.808,0-25,11.193-25,25S-37.808,422-24,422z">
                            </path>
                        </svg>
                    </a>
                @endif
            </div>
        </div>

    </div>

    {{-- Modal --}}
    <div id="customAlertModal" class="fixed inset-0 overflow-y-auto hidden">
        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <!-- Background overlay -->
            <div class="fixed inset-0 transition-opacity" aria-hidden="true">
                <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>

            <!-- Centered modal content -->
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
            <div
                class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                <div class="bg-white p-5">
                    <div class="flex flex-col items-center text-center">
                        <div class="inline-block p-4 bg-teal-50 rounded-full">
                            <svg class="w-12 h-12 fill-current text-teal-500" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 24 24">
                                <path d="M0 0h24v24H0V0z" fill="none" />
                                <path
                                    d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z" />
                            </svg>
                        </div>
                        <h2 class="mt-2 font-semibold text-gray-800" id="modalTitle">Warning</h2>
                        <p class="mt-2 text-sm text-gray-600 leading-relaxed" id="modalContent">
                            Silahkan memilih jawaban terlebih dahulu.
                        </p>
                    </div>
                </div>
                <div class="bg-gray-100 p-4">
                    <button onclick="closeAlertModal()"
                        class="w-full px-4 py-2 bg-teal-500 hover:bg-teal-600 text-white text-sm font-medium rounded-md">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        // animasi
        document.addEventListener('DOMContentLoaded', function() {
            var transitionElements = document.querySelectorAll('.transition-effect');

            // Tambahkan kelas "active" saat halaman dimuat
            transitionElements.forEach(function(element) {
                element.classList.add('active');
            });
        });


        // animasi quiz
        function checkAnswer(button, selectedAnswer, correctAnswer, correctOption, questionId) {
            console.log('Selected Answer:', selectedAnswer);
            var errorMessageId = 'error-message-' + questionId;
            var errorMessageElement = document.getElementById(errorMessageId);

            if (button.getAttribute('data-disabled') === 'true') {
                return;
            }

            var buttons = document.querySelectorAll('[data-question-id="' + questionId + '"]');
            buttons.forEach(function(btn) {
                if (btn === button) {
                    btn.style.opacity = 1;
                    btn.classList.add(selectedAnswer === correctAnswer ? 'correct-answer' : 'wrong-answer');
                    hasAnswerSelected = true; // Set the flag to true when an answer is selected
                } else {
                    btn.style.opacity = 0.5;
                }

                btn.setAttribute('disabled', 'true');
                btn.setAttribute('data-disabled', 'true');
            });

            var errorIcon = selectedAnswer !== correctAnswer ? '❌' : '🎉';
            var errorMessage = selectedAnswer !== correctAnswer ? "Salah, jawaban yang benar adalah '" + correctAnswer :
                "Jawabanmu benar!";

            errorMessageElement.innerHTML = `<div class="error-icon">${errorIcon}</div>${errorMessage}`;
            errorMessageElement.style.color = selectedAnswer !== correctAnswer ? 'red' : 'green';
        }
    </script>

</body>

</html>

<div class="flex flex-col">
    <div class="transition-transform ease-in-out duration-500 flex flex-col gap-10" wire:transition.slide.left>
        <div class="h-[250px] w-[400px] overflow-hidden">
            <img src="{{ asset('/storage/bahasas/' . $bahasas[$index]->image) }}" class="w-full h-full object-cover">
        </div>
        <p class="text-[36px] font-league text-[#273041] font-bold leading-tight">{{ $bahasas[$index]->bahasa }}</p>
    </div>

    <div class="flex gap-20 py-5">
        <button wire:click="previous" class="w-10">
            <svg viewBox="0 0 100 100">
                <path d="M 25,50 L 60,90 L 65,85 L 35,50  L 65,15 L 60,10 Z" class="arrow"></path>
            </svg>
        </button>
        <button wire:click="next" class="w-10">
            <svg viewBox="0 0 100 100">
                <path d="M 25,50 L 60,90 L 65,85 L 35,50  L 65,15 L 60,10 Z" class="arrow"
                    transform="translate(100, 100) rotate(180) "></path>
            </svg>
        </button>
    </div>
</div>
<!-- resources/views/livewire/language-carousel.blade.php -->

<script>
    document.addEventListener('livewire:load', function() {
        Livewire.hook('message.processed', (message, component) => {
            if (message.updateQueue[0].method === 'updatedIndex') {
                const element = document.querySelector('[wire:transition.slide.left]');
                element.classList.add('transform', 'translate-x-0');
                setTimeout(() => {
                    element.classList.remove('transform', 'translate-x-0');
                }, 500);
            }
        });
    });
</script>

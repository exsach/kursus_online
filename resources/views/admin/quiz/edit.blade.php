<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Edit - Quiz - Golingua</title>
</head>

<body>
    @extends('admin.sidebar.index')

    @section('content')
        <form action="{{ route('quiz.update', $quiz->id) }}" method="POST" enctype="multipart/form-data"
            class="bg-white p-5 shadow rounded-lg">
            @csrf
            @method('PUT')
            <div class="flex gap-5">
                <div class="mb-6 w-full">
                    <label for="teks_soal" class="block mb-2 text-sm font-medium text-gray-900 uppercase">soal</label>
                    <input type="text" id="teks_soal"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="teks_soal" value="{{ old('teks_soal', $quiz->teks_soal) }}" placeholder="Masukkan Soal"
                        required>
                </div>
                <div class="mb-6 w-full">
                    <label for="opsi_a" class="block mb-2 text-sm font-medium text-gray-900 ">opsi a</label>
                    <input type="text" id="opsi_a"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="opsi_a" value="{{ old('opsi_a', $quiz->opsi_a) }}" placeholder="Masukkan Opsi A" required>
                </div>
                <div class="mb-6 w-full">
                    <label for="opsi_b" class="block mb-2 text-sm font-medium text-gray-900 ">opsi b</label>
                    <input type="text" id="opsi_b"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="opsi_b" value="{{ old('opsi_b', $quiz->opsi_b) }}" placeholder="Masukkan Opsi B" required>
                </div>
                <div class="mb-6 w-full">
                    <label for="opsi_c" class="block mb-2 text-sm font-medium text-gray-900 ">opsi c</label>
                    <input type="text" id="opsi_c"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="opsi_c" value="{{ old('opsi_c', $quiz->opsi_c) }}" placeholder="Masukkan Opsi C" required>
                </div>
                <div class="mb-6 w-full">
                    <label for="opsi_d" class="block mb-2 text-sm font-medium text-gray-900 ">opsi d</label>
                    <input type="text" id="opsi_d"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="opsi_d" value="{{ old('opsi_d', $quiz->opsi_d) }}" placeholder="Masukkan Opsi D" required>
                </div>
            </div>
            <div class="flex gap-5">
                <div class="mb-6 w-full">
                    <div class="mb-6 w-full">
                        <label for="jawaban" class="block mb-2 text-sm font-medium text-gray-900 uppercase">jawaban</label>
                        <input type="text" id="jawaban"
                            class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                            name="jawaban" placeholder="Masukkan jawaban" value="{{ old('jawaban', $quiz->jawaban) }}" required>
                    </div>
                </div>
                <div class="mb-6 w-full">
                    <label for="skor" class="block mb-2 text-sm font-medium text-gray-900 uppercase">skor</label>
                    <input type="number" id="skor"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="skor" value="{{ old('skor', $quiz->skor) }}" placeholder="Masukkan Skor" required>
                </div>
                <div class="mb-6 w-full">
                    <label for="image" class="block mb-2 text-sm font-medium text-gray-900">IMAGE</label>
                    <input
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-pointer"
                        name="image" aria-describedby="user_avatar_help" id="user_avatar" type="file">
                </div>
                <select name="parent_id"
                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                    <option value="">Pilih Parent (Opsional)</option>
                    @foreach ($parents as $parent)
                        <option value="{{ $parent->id }}"
                            {{ old('parent_id', $quiz->parent_id) == $parent->id ? 'selected' : '' }}>
                                {{ $parent->nomor_level }} {{ $parent->materi->judul }}
                        </option>
                    @endforeach
                </select>
            </div>
            <button type="submit"
                class="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">SIMPAN</button>
            <button type="reset"
                class="text-white bg-yellow-500 hover:bg-yellow-600 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">RESET</button>
        </form>
    @endsection

</body>

</html>

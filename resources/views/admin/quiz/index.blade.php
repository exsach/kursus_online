<!DOCTYPE html>
<html lang="en" class="antialiased">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Quiz - GoLingua</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;300;500;700;900&display=swap"
        rel="stylesheet">

    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--Regular Datatables CSS-->
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <!--Responsive Extension Datatables CSS-->
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">

    <style>
        .font-league {
            font-family: 'League Spartan', sans-serif;
        }

        /*Form fields*/
        .dataTables_wrapper select,
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568;
            /*text-gray-700*/
            padding-left: 1rem;
            /*pl-4*/
            padding-right: 1rem;
            /*pl-4*/
            padding-top: .5rem;
            /*pl-2*/
            padding-bottom: .5rem;
            /*pl-2*/
            line-height: 1.25;
            /*leading-tight*/
            border-width: 2px;
            /*border-2*/
            border-radius: .25rem;
            border-color: #edf2f7;
            /*border-gray-200*/
            background-color: #edf2f7;
            /*bg-gray-200*/
        }

        /*Row Hover*/
        table.dataTable.hover tbody tr:hover,
        table.dataTable.display tbody tr:hover {
            background-color: #ebf4ff;
            /*bg-indigo-100*/
        }

        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button {
            font-weight: 700;
            /*font-bold*/
            border-radius: .25rem;
            /*rounded*/
            border: 1px solid transparent;
            /*border border-transparent*/
        }

        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            color: #fff !important;
            /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06);
            /*shadow*/
            font-weight: 700;
            /*font-bold*/
            border-radius: .25rem;
            /*rounded*/
            background: #667eea !important;
            /*bg-indigo-500*/
            border: 1px solid transparent;
            /*border border-transparent*/
        }

        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff !important;
            /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06);
            /*shadow*/
            font-weight: 700;
            /*font-bold*/
            border-radius: .25rem;
            /*rounded*/
            background: #667eea !important;
            /*bg-indigo-500*/
            border: 1px solid transparent;
            /*border border-transparent*/
        }

        /*Add padding to bottom border */
        table.dataTable.no-footer {
            border-bottom: 1px solid #e2e8f0;
            /*border-b-1 border-gray-300*/
            margin-top: 0.75em;
            margin-bottom: 0.75em;
        }

        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before,
        table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
            background-color: #667eea !important;
            /*bg-indigo-500*/
        }
    </style>



</head>

<body class="bg-gray-100 text-gray-900 tracking-wider leading-normal">
    @extends('admin.sidebar.index')

    @section('content')
        <!--Container-->
        <div class="container w-full  mx-auto px-2 ">

            <div class="flex justify-between px-2 py-8">
                <h1 class="flex items-center font-sans font-bold break-normal text-indigo-500 text-xl md:text-2xl">
                    Data Quiz
                </h1>
                <a href="{{ route('quiz.create') }}" class="bg-indigo-500 px-4 py-2 rounded-md text-white">create</a>
            </div>


            <!--Card-->
            <div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
                <table id="example" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                    <thead>
                        <tr>
                            <th data-priority="1">ID</th>
                            <th data-priority="2">Soal</th>
                            <th data-priority="3">Opsi A</th>
                            <th data-priority="4">Opsi B</th>
                            <th data-priority="5">Opsi C</th>
                            <th data-priority="6">Opsi D</th>
                            <th data-priority="7">Jawaban</th>
                            <th data-priority="8">Skor</th>
                            <th data-priority="9">Gambar</th>
                            <th data-priority="10">Level</th>
                            <th data-priority="11"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($quizzes as $quiz)
                            <tr>
                                <td class="text-center">
                                    {{ $quiz->id }}
                                </td>
                                <td>
                                    {{ $quiz->teks_soal }}
                                </td>
                                <td class="text-center">
                                    {{ $quiz->opsi_a }}
                                </td>
                                <td class="text-center">
                                    {{ $quiz->opsi_b }}
                                </td>
                                <td class="text-center">
                                    {{ $quiz->opsi_c }}
                                </td>
                                <td class="text-center">
                                    {{ $quiz->opsi_d }}
                                </td>
                                <td class="text-center">
                                    {{ $quiz->jawaban }}
                                </td>
                                <td class="text-center">
                                    {{ $quiz->skor }}
                                </td>
                                <td class="text-center">
                                    @if ($quiz->image)
                                        <img src="{{ asset('/storage/quiz/' . $quiz->image) }}" style="width: 50px"
                                            class="mx-auto rounded">
                                    @else
                                    @endif
                                </td>
                                <td class="text-center">{{ optional($quiz->level)->nomor_level }}</td>
                                <td class="flex justify-center items-center gap-2">
                                    <a href="{{ route('quiz.edit', ['quiz' => $quiz->id]) }}"
                                        class="bg-yellow-500 px-5 py-2 rounded-md text-white">Edit</a>
                                    <form action="{{ route('quiz.destroy', ['quiz' => $quiz->id]) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit"
                                            class="bg-red-500 px-4 py-2 rounded-md text-white">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
        <!--/container-->

        <!-- jQuery -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

        <!--Datatables -->
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script>
            $(document).ready(function() {
                var table = $('#example').DataTable({
                    responsive: true,
                    lengthMenu: [5, 10, 25, 50], // Set the available options for entries per page
                    pageLength: 5 // Set the default number of rows to display per page
                });

                // Adjust and recalculate responsiveness
                table.columns.adjust().responsive.recalc();
            });
        </script>
    @endsection

</body>

</html>

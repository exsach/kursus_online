<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Create - User - Golingua</title>
</head>

<body>
    @extends('admin.sidebar.index')

    @section('content')
        <form action="{{ route('user.update', ['user' => $user->id]) }}" method="post" enctype="multipart/form-data"
            class="bg-white p-5 shadow rounded-lg">
            @csrf
            @method('PUT')
            <div class="flex gap-5">
                <div class="mb-6 w-full">
                    <label for="user" class="block mb-2 text-sm font-medium text-gray-900 ">NAME</label>
                    <input type="text" id="user"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 @error('name') is-invalid @enderror"
                        name="name" value="{{ old('name', $user->name) }}" placeholder="Masukkan Nama" required>

                    <!-- error message untuk name -->
                    @error('name')
                        <div class="mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-6 w-full">
                    <label for="email" class="block mb-2 text-sm font-medium text-gray-900 ">EMAIL</label>
                    <input type="text" id="email"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email', $user->email) }}" placeholder="Masukkan Email" required>

                    <!-- error message untuk name -->
                    @error('email')
                        <div class="mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-6 w-full">
                    <label for="profil_image" class="block mb-2 text-sm font-medium text-gray-900">PROFIL IMAGE</label>
                    <input type="file" id="profil_image" name="image"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-pointer @error('profil_image') is-invalid @enderror"
                        aria-describedby="user_avatar_help">


                    @error('profil_image')
                        <div class="mt-2 text-red-500">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>

            <div class="flex gap-5">
                <div class="mb-6 w-1/2">
                    <label for="role" class="block mb-2 text-sm font-medium text-gray-900 ">ROLE</label>
                    <select id="role" name="role"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                        <option value="admin" @if (old('role', $user->role) === 'admin') selected @endif>Admin</option>
                        <option value="user" @if (old('role', $user->role) === 'user') selected @endif>User</option>
                    </select>
                </div>

                <div class="mb-6 w-1/2">
                    <label for="language_id" class="block mb-2 text-sm font-medium text-gray-900">LANGUAGE</label>
                    <select name="language_id"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                        @foreach ($languages as $id => $bahasa)
                            <option value="{{ $id }}">{{ $bahasa }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit"
                class="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">SIMPAN</button>
            <button type="reset"
                class="text-white bg-yellow-500 hover:bg-yellow-600 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">RESET</button>
        </form>
    @endsection

</body>

</html>

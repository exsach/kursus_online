<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Create - Bahasa - Golingua</title>
</head>

<body>
    @extends('admin.sidebar.index')

    @section('content')
        <form action="{{ route('menu.update', $menu->id) }}" method="POST" enctype="multipart/form-data"
            class="bg-white p-5 shadow rounded-lg">
            @csrf
            @method('PUT')
            <div class="flex gap-5">
                <div class="mb-6 w-1/2">
                    <label for="label" class="block mb-2 text-sm font-medium text-gray-900 ">LABEL</label>
                    <input type="text" id="label"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 @error('label') is-invalid @enderror"
                        name="label" value="{{ old('label', $menu->label) }}" placeholder="Masukkan label" required>

                    <!-- error message untuk label -->
                    @error('label')
                        <div class="mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-6 w-1/2">
                    <label for="route" class="block mb-2 text-sm font-medium text-gray-900 ">ROUTE</label>
                    <input type="text" id="route"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 @error('route') is-invalid @enderror"
                        name="route" value="{{ old('route', $menu->route) }}" placeholder="Masukkan route" required>

                    <!-- error message untuk route -->
                    @error('route')
                        <div class="mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>

            <div class="flex gap-5">
                <div class="mb-6 w-1/2">
                    <label for="id" class="block mb-2 text-sm font-medium text-gray-900">ICON</label>
                    <input
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-pointer @error('image') is-invalid @enderror"
                        name="image" aria-describedby="user_avatar_help" id="user_avatar" type="text">

                    @error('image')
                        <div class="mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-6 w-1/2">
                    <label for="parent_id" class="block mb-2 text-sm font-medium text-gray-900">Parent Menu
                        (Opsional)</label>
                    <select name="parent_id"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                        <option value="">Pilih Parent (Opsional)</option>
                        @foreach ($parent as $id => $label)
                            <option value="{{ $id }}" @if ($id == $menu->parent_id) selected @endif>
                                {{ $label }}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            <button type="submit"
                class="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">SIMPAN</button>
            <button type="reset"
                class="text-white bg-yellow-500 hover:bg-yellow-600 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">RESET</button>
        </form>
    @endsection

</body>

</html>

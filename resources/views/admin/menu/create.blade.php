<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Create - Bahasa - Golingua</title>
</head>

<body>
    @extends('admin.sidebar.index')

    @section('content')
    <form action="{{ route('menu.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="flex gap-5">
        <div class="mb-6 w-1/2">
            <label for="label" class="block mb-2 text-sm font-medium text-gray-900">Label</label>
            <input type="text" id="label"
                class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                name="label" placeholder="Masukkan Label" required>
        </div>
    
        <div class="mb-6 w-1/2">
            <label for="route" class="block mb-2 text-sm font-medium text-gray-900">Route</label>
            <input type="text" id="route"
                class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                name="route" placeholder="Masukkan Route" required>
        </div>
        </div>
    
        <div class="flex gap-5">
        <div class="mb-6 w-1/2">
            <label for="image" class="block mb-2 text-sm font-medium text-gray-900">Icon</label>
            <input class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-pointer"
                name="image" id="image" type="text" aria-describedby="user_avatar_help">
        </div>
    
        <div class="mb-6 w-1/2">
            <label for="parent_id" class="block mb-2 text-sm font-medium text-gray-900">Parent Menu (Opsional)</label>
            <select name="parent_id"
                class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                <option value="">Pilih Parent (Opsional)</option>
                @foreach ($parent as $id => $bahasa)
                    <option value="{{ $id }}">{{ $bahasa }}</option>
                @endforeach
            </select>
        </div>
        </div>
    
        <div class="mb-6">
            <button type="submit"
                class="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">Simpan</button>
            <button type="reset"
                class="text-white bg-yellow-500 hover:bg-yellow-600 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">Reset</button>
        </div>
    </form>
    @endsection

</body>

</html>

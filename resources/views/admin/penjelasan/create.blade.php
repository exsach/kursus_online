<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Create - Penjelasan - Golingua</title>
</head>

<body>
    @extends('admin.sidebar.index')

    @section('content')
        <form action="{{ route('penjelasan.store') }}" method="POST" enctype="multipart/form-data"
            class="bg-white p-5 shadow rounded-lg">
            @csrf
            <div class="flex gap-5">
                <div class="mb-6 w-full">
                    <label for="judul" class="block mb-2 text-sm font-medium text-gray-900 ">JUDUL</label>
                    <input type="text" id="judul"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="judul" placeholder="Masukkan Judul" required>
                </div>
                <div class="mb-6 w-full">
                    <label for="isi" class="block mb-2 text-sm font-medium text-gray-900 ">ISI</label>
                    <textarea id="isi"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="isi" placeholder="Masukkan Isi"></textarea>
                </div>
                <div class="mb-6 w-full">
                    <label for="contoh" class="block mb-2 text-sm font-medium text-gray-900">CONTOH</label>
                    <textarea id="contoh"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="contoh" placeholder="Masukkan Contoh" wrap="off"></textarea>
                </div>                
                <div class="mb-6 w-full">
                    <label for="image" class="block mb-2 text-sm font-medium text-gray-900">IMAGE</label>
                    <input
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-pointer"
                        name="image" id="image" type="file">
                </div>

                <select name="parent_id"
                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                    <option value="">Pilih Parent (Opsional)</option>
                    @foreach ($parents as $parent)
                        <option value="{{ $parent->id }}">
                            {{ $parent->nomor_level }} {{ $parent->materi->judul }}
                        </option>
                    @endforeach
                </select>
            </div>
            <button type="submit"
                class="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">SIMPAN</button>
            <button type="reset"
                class="text-white bg-yellow-500 hover:bg-yellow-600 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">RESET</button>
        </form>
    @endsection

</body>

</html>

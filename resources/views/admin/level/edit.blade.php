<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Edit - Level - Golingua</title>
</head>

<body>
    @extends('admin.sidebar.index')

    @section('content')
        <form action="{{ route('level.update', $level->id) }}" method="POST" enctype="multipart/form-data"
            class="bg-white p-5 shadow rounded-lg">
            @csrf
            @method('PUT')
            <div class="flex gap-5">
                <div class="mb-6 w-full">
                    <label for="nomor_level" class="block mb-2 text-sm font-medium text-gray-900 ">NOMOR_LEVEL</label>
                    <input type="number" id="nomor_level"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="nomor_level" value="{{ old('nomor_level', $level->nomor_level) }}" placeholder="Masukkan Nomor Level" required>
                </div>
                <div class="mb-6 w-full">
                    <label for="status" class="block mb-2 text-sm font-medium text-gray-900">STATUS</label>
                    <select id="status" name="status" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required>
                        <option value="terbuka" {{ old('status') === 'terbuka' ? 'selected' : '' }}>Terbuka</option>
                        <option value="terkunci" {{ old('status') === 'terkunci' ? 'selected' : '' }}>Terkunci</option>
                        <option value="selesai" {{ old('status') === 'selesai' ? 'selected' : '' }}>Selesai</option>
                        <option value="belum selesai" {{ old('status') === 'belum selesai' ? 'selected' : '' }}>Belum Selesai</option>
                    </select>
                </div>
                <select name="parent_id" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                    <option value="">Pilih Parent (Opsional)</option>
                    @foreach ($parents as $parent)
                        <option value="{{ $parent->id }}" {{ old('parent_id', $level->parent_id) == $parent->id ? 'selected' : '' }}>
                            {{ $parent->judul }}
                        </option>
                    @endforeach
                </select>
            </div>
            <button type="submit"
                class="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">SIMPAN</button>
            <button type="reset"
                class="text-white bg-yellow-500 hover:bg-yellow-600 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">RESET</button>
        </form>
    @endsection

</body>

</html>
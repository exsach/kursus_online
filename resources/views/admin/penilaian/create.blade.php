<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Create - Penilaian - Golingua</title>
</head>

<body>
    @extends('admin.sidebar.index')

    @section('content')
        <form action="{{ route('penilaian.store') }}" method="POST" enctype="multipart/form-data"
            class="bg-white p-5 shadow rounded-lg">
            @csrf
            <div class="flex gap-5">
                <div class="mb-6 w-full">
                    <label for="user_id" class="block mb-2 text-sm font-medium text-gray-900 uppercase">NAMA USER</label>
                    <select id="user_id"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="user_id" required>
                        <option value="">Pilih User</option>
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>                
                <div class="mb-6 w-full">
                    <label for="quiz_id" class="block mb-2 text-sm font-medium text-gray-900 uppercase">SOAL KUIS</label>
                    <select id="quiz_id"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="quiz_id" required>
                        <option value="">Pilih Quiz</option>
                        @foreach ($quizzes as $quiz)
                            <option value="{{ $quiz->id }}">{{ $quiz->teks_soal }}</option>
                        @endforeach
                    </select>
                </div>      
                <div class="mb-6 w-full">
                    <label for="jawaban" class="block mb-2 text-sm font-medium text-gray-900 uppercase">Jawaban</label>
                    <select id="jawaban" name="jawaban" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                    </select>
                </div>          
                <div class="mb-6 w-full">
                    <label for="status" class="block mb-2 text-sm font-medium text-gray-900 uppercase">status</label>
                    <select id="status" name="status" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                        <option value="">Pilih Status</option>
                        <option value="benar">benar</option>
                        <option value="salah">salah</option>
                    </select>
                </div>
                <div class="mb-6 w-full">
                    <label for="skor" class="block mb-2 text-sm font-medium text-gray-900 uppercase">skor</label>
                    <input type="number" id="skor"
                        class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        name="skor" placeholder="Masukkan Skor">
                </div>
            </div>
            <button type="submit"
                class="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">SIMPAN</button>
            <button type="reset"
                class="text-white bg-yellow-500 hover:bg-yellow-600 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">RESET</button>
        </form>
    @endsection

</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Penilaian - GoLingua</title>
</head>

<body>
    @extends('admin.sidebar.index')

    @section('content')
    <button type="button"
        class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">
        <a href="{{ route('penilaian.create') }}">create</a>
    </button>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left text-gray-500 ">
                <thead class="text-xs text-gray-700 uppercase bg-gray-200">
                    <tr>
                        <th scope="col" class="px-6 py-3 uppercase text-center">ID</th>
                        <th scope="col" class="px-6 py-3 uppercase text-center">NAMA USER</th>
                        <th scope="col" class="px-6 py-3 uppercase text-center">SOAL KUIS</th>
                        <th scope="col" class="px-6 py-3 uppercase text-center">JAWABAN</th>
                        <th scope="col" class="px-6 py-3 uppercase text-center">STATUS</th>
                        <th scope="col" class="px-6 py-3 uppercase text-center">SKOR</th>
                        <th scope="col" class="px-6 py-3">
                            <span class="sr-only">Edit</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($penilaians as $penilaian)

                        <tr class="bg-white border-b ">
                            <th scope="row" class="px-6 py-4 font-medium  whitespace-nowrap text-center">
                                {{ $penilaian->id }}
                            </th>
                            <td class="px-6 py-4 text-center">{{ optional($penilaian->user)->name }}</td>  
                            <td class="px-6 py-4 text-center">{{ optional($penilaian->quiz)->teks_soal }}</td>  
                            <td class="px-6 py-4 text-center">
                                {{ $penilaian->jawaban }}
                            </td>                         
                            <td class="px-6 py-4 text-center">
                                {{ $penilaian->status }}
                            </td>                         
                            <td class="px-6 py-4 text-center">
                                {{ $penilaian->skor }}
                            </td> 
                            <td class="px-6 py-4 text-right">
                                <div>
                                    <a href="{{ route('penilaian.edit', ['penilaian' => $penilaian->id]) }}"
                                        class="font-medium text-blue-600 hover:underline">Edit</a>
                                    <form action="{{ route('penilaian.destroy', ['penilaian' => $penilaian->id]) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit"
                                            class="font-medium text-red-600 hover:underline">Hapus</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <div>
                            Empty Data.
                        </div>
                    @endforelse

                </tbody>
            </table>
            <div class="border-none">
                {{ $penilaians->links() }}
            </div>
        </div>
    @endsection

</body>

</html>

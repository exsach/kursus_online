<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>Golingua</title>
</head>

<body>
    @extends('admin.sidebar.index')

    @section('content')
        {{-- <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!-- Other dashboard content here -->
            </div>
            <div class="col-md-4">
                <canvas id="userRegistrationChart" width="400" height="200"></canvas>
            </div>
        </div>
    </div> --}}
        <div>
            dashboard
        </div>
    @endsection
</body>

</html>

<?php

namespace App\Livewire;

use Livewire\Component;

class LanguageCarousel extends Component
{
    public $index = 0;
    public $bahasas;

    public function mount($bahasas)
    {
        $this->bahasas = $bahasas;
    }

    public function previous()
    {
        $count = count($this->bahasas);
        $this->index = ($this->index - 1 + $count) % $count;
    }
    
    public function next()
    {
        $this->index = ($this->index + 1) % count($this->bahasas);
    }

    public function updatedIndex()
    {
        $this->dispatchBrowserEvent('slide-changed');
    }

    public function render()
    {
        return view('livewire.language-carousel');
    }
}
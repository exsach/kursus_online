<?php

namespace App\Providers;

use App\Models\Menu;
use App\Models\User;
use App\Models\Bahasa;
use App\Models\Package;
use App\Models\Transaksi;
use App\Models\Penjelasan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

        $menus = Menu::all();
        view()->share('menus', $menus);

        $sidebarMenus = Menu::with('children')
        ->whereNull('parent_id')
        ->orWhere('parent_id', '>=', 1)
        ->get();
        view()->share('sidebarMenus', $sidebarMenus);

        View::share('bahasas', Bahasa::get());
        View::share('penjelasans', Penjelasan::get());  
    }
}
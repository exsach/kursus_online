<?php

namespace App\Models;

use App\Models\Bahasa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Materi extends Model
{
    use HasFactory;

    protected $table = 'materis';
    protected $fillable = [
        'judul',
        'image',
        'parent_id'
    ];

    // menjadi child dari bahasa
    public function bahasa()
    {
        return $this->belongsTo(Bahasa::class, 'parent_id');
    }

    // menjadi parent dari level
    public function level()
    {
        return $this->hasMany(Level::class, 'parent_id');
    }
}
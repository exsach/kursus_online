<?php

namespace App\Models;

use App\Models\Materi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bahasa extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'id',
        'bahasa',
        'image'
    ];

    public function materi()
    {
        return $this->hasMany(Materi::class, 'parent_id');
    }

    public function user()
    {
        return $this->hasMany(User::class, 'user_id');
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    use HasFactory;

    protected $fillable = [
        'teks_soal',
        'opsi_a',
        'opsi_b',
        'opsi_c',
        'opsi_d',
        'jawaban',
        'skor',
        'parent_id',
        'image'
    ];

    public function level()
    {
        return $this->belongsTo(Level::class, 'parent_id');
    }
    public function penilaian()
    {
        return $this->hasMany(Penilaian::class, 'quiz_id');
    }
}
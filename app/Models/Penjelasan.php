<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penjelasan extends Model
{
    use HasFactory;

    protected $fillable = [
        'judul',
        'isi',
        'contoh',
        'parent_id',
        'image'
    ];

    public function level()
    {
        return $this->belongsTo(Level::class, 'parent_id');
    }
}
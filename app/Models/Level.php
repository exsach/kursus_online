<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use HasFactory;

    protected $fillable = [
        'nomor_level',
        'status',
        'parent_id'
    ];

    // menjadi child dari materi
    public function materi()
    {
        return $this->belongsTo(Materi::class, 'parent_id');
    }

    // menjadi parent dari penjelasan
    public function penjelasan()
    {
        return $this->hasMany(Penjelasan::class, 'parent_id');
    }
    public function quiz()
    {
        return $this->hasMany(Quiz::class, 'parent_id');
    }
}
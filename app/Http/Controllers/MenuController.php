<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $menus = Menu::with('children')
            ->whereNull('parent_id') // Menampilkan menu yang tidak memiliki parent
            ->orWhere('parent_id', '>=', 1) // Menampilkan menu dengan parent_id lebih besar atau sama dengan 1
            ->get();

        // Di sidebar (tanpa paginasi)
        $sidebarMenus = Menu::with('children')
            ->whereNull('parent_id')
            ->orWhere('parent_id', '>=', 1)
            ->get();

        return view('admin.menu.index', compact('menus', 'sidebarMenus'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $parent = Menu::whereNull('parent_id')->pluck('label', 'id');
        return view('admin.menu.create', compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'label'     => 'required|string',
            'route'     => 'required',
            'image'     => 'required|string',
            'parent_id' => 'nullable|exists:menus,id',
        ]);

        // $image = $request->file('image');
        // $image->storeAs('public/menus', $image->hashName());

        Menu::create([
            'label' => $request->label,
            'route' => $request->route,
            'image' =>  $request->image,
            'parent_id' => $request->input('parent_id'),
        ]);

        if ($request->input('parent_id')) {
            $activeParent = Menu::find($request->input('parent_id'));
            $activeParent->update(['active' => true]);
        }

        return redirect()->route('menu.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Menu $menu)
    {
        $parent = Menu::whereNull('parent_id')->pluck('label', 'id');
        return view('admin.menu.edit', compact('menu', 'parent'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Menu $menu)
    {
        $this->validate($request, [
            'label'     => 'required|string',
            'route'     => 'required',
            'image'     => 'nullable',
            'parent_id' => 'nullable|exists:menus,id',
        ]);

        $existingImage = $menu->image;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image->storeAs('public/menus', $image->hashName());

            if ($existingImage) {
                Storage::delete('public/menus/' . $existingImage);
            }

            $menu->update([
                'label'     => $request->label,
                'route'     => $request->route,
                'image'     => $request->image,
                'parent_id' => $request->input('parent_id'),
            ]);
        } else {
            $menu->update([
                'label'     => $request->label,
                'route'     => $request->route,
                'image'     => $request->image,
                'parent_id' => $request->input('parent_id'),
            ]);
        }

        if ($request->input('parent_id')) {
            $activeParent = Menu::find($request->input('parent_id'));
            $activeParent->update(['active' => true]);
        }

        return redirect()->route('menu.index')->with(['success' => 'Data Berhasil di Update!']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Menu $menu)
    {
        Storage::delete('public/menus/' . $menu->image);
        $menu->delete();
        return redirect()->route('menu.index')->with(['success' => 'Data Berhasil di Delete']);
    }

    public function children()
    {
        return $this->hasMany(Menu::class, 'parent_id');
    }
}
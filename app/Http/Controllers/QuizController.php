<?php

namespace App\Http\Controllers;

use App\Models\Quiz;
use App\Models\Level;
use App\Models\Penjelasan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $quizzes = Quiz::with('level')->paginate(5);
        return view('admin.quiz.index', compact('quizzes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $parents = Level::with('quiz')->get();
        return view('admin.quiz.create', compact('parents'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'teks_soal' => 'required',
            'opsi_a' => 'required',
            'opsi_b' => 'required',
            'opsi_c' => 'required',
            'opsi_d' => 'required',
            'jawaban' => 'required',
            'skor' => 'required',
            'image' => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
            'parent_id' => 'nullable',
        ]);

        $image = $request->file('image');

        // Check if image is provided
        if ($image) {
            $image->storeAs('public/quiz', $image->hashName());
        }

        $quiz = Quiz::create([
            'teks_soal' => $request->teks_soal,
            'opsi_a' => $request->opsi_a,
            'opsi_b' => $request->opsi_b,
            'opsi_c' => $request->opsi_c,
            'opsi_d' => $request->opsi_d,
            'jawaban' => $request->jawaban,
            'skor' => $request->skor,
            'image' => $image ? $image->hashName() : null,
            'parent_id' => $request->input('parent_id'),
        ]);

        if ($request->input('parent_id') && $quiz) {
            $activeParent = Quiz::find($request->input('parent_id'));

            // Check if the parent record is found
            if ($activeParent) {
                $activeParent->update(['active' => true]);
            }
        }

        return redirect()->route('quiz.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }


    /**
     * Display the specified resource.
     */
    public function show(Quiz $quiz)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Quiz $quiz)
    {
        $parents = Level::with('quiz')->get();
        return view('admin.quiz.edit', compact('quiz', 'parents'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Quiz $quiz)
    {
        $this->validate($request, [
            'teks_soal'     => 'required',
            'opsi_a'     => 'required',
            'opsi_b'     => 'required',
            'opsi_c'     => 'required',
            'opsi_d'     => 'required',
            'jawaban'     => 'required',
            'skor'     => 'required',
            'image'      => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
            'parent_id' =>  'nullable',
        ]);

        $existingImage = $quiz->image;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image->storeAs('public/quiz', $image->hashName());

            //hapus gambar lama
            if ($existingImage) {
                Storage::delete('public/quiz/' . $existingImage);
            }

            $quiz->update([
                'teks_soal'    => $request->teks_soal,
                'opsi_a'    => $request->opsi_a,
                'opsi_b'    => $request->opsi_b,
                'opsi_c'    => $request->opsi_c,
                'opsi_d'    => $request->opsi_d,
                'jawaban'    => $request->jawaban,
                'skor'    => $request->skor,
                'image'     => $image->hashName(),
                'parent_id' => $request->input('parent_id'),
            ]);
        } else {
            $quiz->update([
                'teks_soal'    => $request->teks_soal,
                'opsi_a'    => $request->opsi_a,
                'opsi_b'    => $request->opsi_b,
                'opsi_c'    => $request->opsi_c,
                'opsi_d'    => $request->opsi_d,
                'jawaban'    => $request->jawaban,
                'skor'    => $request->skor,
                'image'     => $existingImage,
                'parent_id' => $request->input('parent_id'),
            ]);
        }

        return redirect()->route('quiz.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Quiz $quiz)
    {
        Storage::delete('public/quiz/' . $quiz->image);
        $quiz->delete();
        return redirect()->route('quiz.index')->with(['success' => 'Data Berhasil di Delete']);
    }

    public function quiz($materiId, $levelId, $page = null)
    {
        $quizz = Quiz::where('parent_id', $levelId)->with('level')->simplePaginate(1);
        $penjelasans = Penjelasan::where('parent_id', $levelId)->with('level')->paginate(1);

        if ($quizz->isEmpty()) {
            return redirect()->route('materi.levels', ['id' => $materiId]);
        }
        $hasMorePages = $quizz->hasMorePages();
        return view('pages.course.quiz', compact('quizz', 'penjelasans', 'materiId', 'levelId', 'page', 'hasMorePages'));
    }
}
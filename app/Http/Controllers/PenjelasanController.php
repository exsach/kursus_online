<?php

namespace App\Http\Controllers;

use App\Models\Quiz;
use App\Models\Level;
use App\Models\Penjelasan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PenjelasanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $penjelasans = Penjelasan::with('level')->paginate(5);
        return view('admin.penjelasan.index', compact('penjelasans'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $parents = Level::with('penjelasan')->get();
        return view('admin.penjelasan.create', compact('parents'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul'     => 'required',
            'isi'       => 'nullable',
            'contoh'    => 'nullable',
            'image'     => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
            'parent_id' => 'nullable|exists:materis,id',
        ]);

        $image = $request->file('image');

        // Check if image is provided
        if ($image) {
            $image->storeAs('public/penjelasans', $image->hashName());
        }

        Penjelasan::create([
            'judul'      => $request->judul,
            'isi'        => $request->isi,
            'contoh'     => $request->contoh,
            'image'      => $image ? $image->hashName() : null,
            'parent_id'  => $request->input('parent_id'),
        ]);

        if ($request->input('parent_id')) {
            $activeParent = Penjelasan::find($request->input('parent_id'));
            $activeParent->update(['active' => true]);
        }

        return redirect()->route('penjelasan.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }


    /**
     * Display the specified resource.
     */
    public function show(Penjelasan $penjelasan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Penjelasan $penjelasan)
    {
        $parents = Level::with('penjelasan')->get();
        return view('admin.penjelasan.edit', compact('penjelasan', 'parents'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Penjelasan $penjelasan)
    {
        $this->validate($request, [
            'judul'     => 'required',
            'isi'     => 'nullable',
            'contoh'     => 'nullable',
            'image'      => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
            'parent_id' =>  'nullable',
        ]);

        $existingImage = $penjelasan->image;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image->storeAs('public/penjelasans', $image->hashName());

            //hapus gambar lama
            if ($existingImage) {
                Storage::delete('public/penjelasans/' . $existingImage);
            }

            $penjelasan->update([
                'judul'    => $request->judul,
                'isi'    => $request->isi,
                'contoh'    => $request->contoh,
                'image'     => $image->hashName(),
                'parent_id' => $request->input('parent_id'),
            ]);
        } else {
            $penjelasan->update([
                'judul'    => $request->judul,
                'isi'    => $request->isi,
                'contoh'    => $request->contoh,
                'image'     => $existingImage,
                'parent_id' => $request->input('parent_id')
            ]);
        }

        return redirect()->route('penjelasan.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Penjelasan $penjelasan)
    {
        Storage::delete('public/penjelasans/' . $penjelasan->image);
        $penjelasan->delete();
        return redirect()->route('penjelasan.index')->with(['success' => 'Data Berhasil di Delete']);
    }

    public function penjelasan($materiId, $levelId, $page = 1)
    {
        $penjelasans = Penjelasan::where('parent_id', $levelId)->with('level')->simplePaginate(1);

        return view('pages.course.pages', compact('penjelasans', 'materiId', 'levelId'));
    }
}
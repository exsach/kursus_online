<?php

namespace App\Http\Controllers;

use App\Mail\ForgotPasswordMail;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterMail;

class AuthController extends Controller
{

    public function register()
    {
        return view('auth.register');
    }

    public function registerPost(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8'
        ]);

        $existingUser = User::where('email', $request->email)->first();

        if ($existingUser) {
            // Jika email sudah terdaftar, kembali ke halaman sebelumnya dengan pesan error
            return redirect()->back()->with('error', 'Email sudah digunakan');
        }

        $save = new User;
        $save->name = trim($request->name);
        $save->email = trim($request->email);
        $save->password = Hash::make($request->password);
        $save->remember_token = Str::random(40);
        $save->save();

        Mail::to($save->email)->send(new RegisterMail($save));
        return redirect('login')->with('success', 'Registrasi berhasil! Silahkan verifikasi akun anda!');

        // $user = new User();

        // $user->name = $request->name;
        // $user->email = $request->email;
        // $user->password = Hash::make($request->password);

        // $user->save();

        // if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
        //     $user = Auth::user();
        //     if ($user->role == 'admin') {
        //         return redirect('/admin/dashboard')->with('success', 'Register and Login Success');
        //     } elseif ($user->role == 'user') {
        //         return redirect('/')->with('success', 'Register and Login Success');
        //     }
        // } else {
        //     return back()->with('error', 'Login Failed');
        // }
    }

    public function verify($token)
    {
        $user = User::where('remember_token', '=', $token)->first();
        if (!empty($user)) {
            $user->email_verified_at = date('Y-m-d H:i:s');
            $user->remember_token = Str::random(40);
            $user->save();

            return redirect('login')->with('success', 'Verifikasi berhasil! Silahkan login terlebih dahulu');
        } else {
            abort(404);
        }
    }
    public function login()
    {
        return view('auth.login');
    }

    public function loginPost(Request $request)
    {

        $remember = !empty($request->remember) ? true : false;
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember)) {
            if (!empty(Auth::user()->email_verified_at)) {
                $user = Auth::user();
                if ($user->role == 'admin') {
                    return redirect('/admin/dashboard')->with('success', 'Login Berhasil');
                } elseif ($user->role == 'user') {
                    return redirect('/')->with('success', 'Login Berhasil');
                }
            } else {

                $user_id = Auth::user()->id;
                Auth::logout();

                $save = User::getSingle($user_id);
                $save->remember_token = Str::random(40);
                $save->save();

                Mail::to($save->email)->send(new RegisterMail($save));
                return redirect()->back()->with('success', 'Silahkan verifikasi akun anda!');
            }
        } else {
            return redirect()->back()->with('error', 'Email atau password salah!');
        }

        // $credetials = [
        //     'email' => $request->email,
        //     'password' => $request->password,
        // ];

        // if (Auth::attempt($credetials)) {
        //     $user = Auth::user();
        //     if ($user->role == 'admin') {
        //         return redirect('/admin/dashboard')->with('success', 'Login Success');
        //     } elseif ($user->role == 'user') {
        //         return redirect('/')->with('success', 'Login Success');
        //     }
        // }

        // return back()->with('error', 'Email or password is incorrect.');
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->role == 'admin') {
            return redirect('/admin/dashboard');
        } elseif ($user->role == 'user') {
            return redirect('/');
        }
    }

    public function forgot()
    {
        return view('auth.forgot');
    }

    public function forgot_password(Request $request)
    {
        $user = User::where('email', '=', $request->email)->first();
        if (!empty($user)) {
            $user->remember_token = Str::random(40);
            $user->save();

            Mail::to($user->email)->send(new ForgotPasswordMail($user));
            return redirect()->back()->with('success', 'Silahkan cek email anda!');
        } else {
            return redirect()->back()->with('error', 'Email tidak ditemukan!');
        }
    }

    public function reset($token)
    {
        $user = User::where('remember_token', '=', $token)->first();
        if (!empty($user)) {
            $data['user'] = $user;
            return view('auth.reset', $data);
        } else {
            return redirect('login');
        }
    }

    public function post_reset($token, Request $request)
    {
        $user = User::where('remember_token', '=', $token)->first();
        if (!empty($user)) {
            if ($request->password == $request->cpassword) {
                $user->password = Hash::make($request->password);
                if (empty($user->email_verified_at)) {
                    $user->email_verified_at = date('Y-m-d H:i:s');
                }
                $user->remember_token = Str::random(40);
                $user->save();
                return redirect('login')->with('success', 'Password berhasil diperbarui');
            } else {
                return redirect()->back()->with('error', 'Password tidak sama!');
            }
        } else {
            abort(404);
        }
    }

    public function password_reset(Request $request)
    {
        $user = Auth::user();

        if (!empty($user)) {
            $data['user'] = $user;
            return view('pages.setting.reset', $data);
        } else {
            return redirect('login');
        }
    }


    public function pw_post_reset(Request $request)
    {
        $user = Auth::user();

        if (!empty($user)) {
            if ($request->password == $request->cpassword) {
                if (strlen($request->password) >= 8) {
                    // Check if the new password is different from the old password
                    if (!Hash::check($request->password, $user->password)) {
                        $user->password = Hash::make($request->password);

                        if (empty($user->email_verified_at)) {
                            $user->email_verified_at = now();
                        }

                        $user->remember_token = Str::random(40);
                        $user->save();

                        return redirect()->back()->with('success', 'Password berhasil diperbarui');
                    } else {
                        return redirect()->back()->with('error', 'Password tidak boleh sama dengan sebelumnya');
                    }
                } else {
                    return redirect()->back()->with('error', 'Password harus minimal 8 karakter');
                }
            } else {
                return redirect()->back()->with('error', 'Password tidak sama!');
            }
        } else {
            abort(404);
        }
    }
}

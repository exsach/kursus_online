<?php

namespace App\Http\Controllers;

use App\Models\Bahasa;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BahasaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $bahasas = Bahasa::latest()->get();
        return view('admin.bahasa.index', compact('bahasas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.bahasa.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        //validate form
        $this->validate($request, [
            'bahasa'     => 'required',
            'image'      => 'required|image|mimes:jpeg,jpg,png'
        ]);

        //upload image
        $image = $request->file('image');
        $image->storeAs('public/bahasas', $image->hashName());

        //create bahasa
        Bahasa::create([
            'bahasa'    => $request->bahasa,
            'image'     => $image->hashName()
        ]);

        // redirect to index
        return redirect()->route('bahasa.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Bahasa $bahasa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Bahasa $bahasa)
    {
        return view('admin.bahasa.edit', compact('bahasa'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Bahasa $bahasa)
    {
        $this->validate($request, [
            'bahasa'     => 'required',
            'image'      => 'nullable|image|mimes:jpeg,jpg,png',
        ]);

        //untuk menyimpan gambar yang ada
        $existingImage = $bahasa->image;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image->storeAs('public/bahasas', $image->hashName());

            //hapus gambar lama
            if ($existingImage) {
                Storage::delete('public/bahasas/' . $existingImage);
            }

            $bahasa->update([
                'bahasa'    => $request->bahasa,
                'image'     => $image->hashName()
            ]);
        } else {
            $bahasa->update([
                'bahasa' => $request->bahasa,
                'image' => $existingImage //gunakan gambar yang sudah ada
            ]);
        }

        return redirect()->route('bahasa.index')->with(['success' => "Data Berhasil di Update!"]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Bahasa $bahasa)
    {
        Storage::delete('public/bahasas/' . $bahasa->image);
        $bahasa->delete(); //delete bahasa
        return redirect()->route('bahasa.index')->with(['success' => 'Data Berhasil di Delete']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Bahasa;
use App\Models\Materi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    public function index(Request $request)
    {
        $searchQuery = $request->input('search');

        $materis = Materi::with('bahasa')
            ->when($searchQuery, function ($query) use ($searchQuery) {
                $query->where('judul', 'like', '%' . $searchQuery . '%');
            })
            ->get();

        return view('pages.course.index', ['materis' => $materis]);
    }


    public function selectLanguage()
    {
        $user = Auth::user();
        $languages = Bahasa::all();
        if ($user->language_id !== null) {
            return redirect()->route('course.index');
        } else {
            return view('pages.course.select-language', compact('languages'));
        }
    }

    public function saveLanguage(Request $request)
    {
        $request->validate([
            'language_id' => 'required|numeric',
        ]);

        $user = Auth::user();
        $userModel = User::find($user->id);
        $userModel->language_id = $request->language_id;
        $userModel->save();

        $selectedLanguage = Bahasa::find($request->language_id);
        session(['selected_language' => $selectedLanguage->bahasa]);

        return redirect()->route('course.index')->with('success', 'Data Berhasil di Simpan');
    }
}
<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\UserController;

class AdminController extends Controller
{
    public function dashboard()
    {
        $adminCount = User::where('role', 'admin')->count();

        return view('admin.dashboard', [
            'adminCount' => $adminCount,
        ]);
    }

    public function index()
    {
        return view('admin.dashboard');
    }
}
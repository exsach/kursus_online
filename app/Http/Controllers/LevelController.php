<?php

namespace App\Http\Controllers;

use App\Models\Level;
use App\Models\Materi;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $levels = Level::with('materi')->get();
        return view('admin.level.index', compact('levels'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $parents = Materi::with('level')->get();
        return view('admin.level.create', compact('parents'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'nomor_level'     => 'required',
            'parent_id' =>  'required|exists:materis,id',
        ]);

        Level::create([
            'nomor_level'    => $request->nomor_level,
            'parent_id' => $request->input('parent_id'),
        ]);

        return redirect()->route('level.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Level $level)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Level $level)
    {
        $parents = Materi::with('level')->get();
        return view('admin.level.edit', compact('level', 'parents'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Level $level)
    {
        $this->validate($request, [
            'nomor_level'     => 'required',
            'parent_id' =>  'nullable|exists:materis,id',
        ]);

        $level->update([
            'nomor_level'    => $request->nomor_level,
            'parent_id' => $request->input('parent_id')
        ]);

        if ($request->input('parent_id')) {
            $activeParent = Level::find($request->input('parent_id'));
            $activeParent->update(['active' => true]);
        }

        return redirect()->route('level.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Level $level)
    {
        // Simpan parent_id dan nomor_level
        $parent_id = $level->parent_id;
        $nomor_level = $level->nomor_level;

        // Hapus level
        $level->delete();

        // Perbarui nomor_level di level-level yang lebih besar
        $levelsToUpdate = Level::where('parent_id', $parent_id)
            ->where('nomor_level', '>', $nomor_level)
            ->get();

        foreach ($levelsToUpdate as $levelToUpdate) {
            $levelToUpdate->nomor_level -= 1;
            $levelToUpdate->save();
        }

        return redirect()->route('level.index')->with(['success' => 'Level berhasil dihapus.']);
    }

    public function showLevels($id)
    {
        $materi = Materi::findOrFail($id);
        $levels = Level::where('parent_id', $materi->id)->orderBy('nomor_level')->get();

        return view('pages.course.level', compact('levels', 'materi'));
    }
}

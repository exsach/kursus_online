<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Bahasa;
use App\Models\Transaksi;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::latest()->get();
        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        $languages = Bahasa::pluck('bahasa', 'id');
        return view('admin.user.edit', compact('user', 'languages'));
    }


    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($id), // Exclude the current user by ID
            ],
            'profile_image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048', // Maksimal 2MB
            'role' => 'required',
            'language_id' => 'required',
        ]);

        $validRoles = ['admin', 'user'];
        if (!in_array($request->role, $validRoles)) {
            return redirect()->back()->with('error', 'Peran yang dimasukkan tidak valid.');
        }

        // Temukan pengguna yang ingin diubah
        $user = User::find($id);

        // Update atribut pengguna jika diperlukan
        if ($user) {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->role = $request->role;
            $user->language_id = $request->language_id;

            if ($request->hasFile('image')) {
                // Hapus gambar lama jika ada
                if ($user->profile_image) {
                    Storage::delete('public/profile_images/' . $user->profile_image);
                }

                // Upload gambar profil yang baru
                $profileImage = $request->file('image');
                $imageName = time() . '.' . 'jpg';
                $profileImage->storeAs('public/profile_images', $imageName);
                $user->profile_image = $imageName;
            }


            $user->save();

            // dd($request->all());
            return redirect()->route('user.index')->with(['success' => "Data Berhasil di Update!"]);
        } else {
            return redirect()->route('user.index')->with(['error' => "Pengguna tidak ditemukan."]);
        }
    }

    public function destroy($id)
    {
        $user = User::find($id);

        if ($user) {
            // Hapus gambar profil jika ada
            if ($user->profile_image) {
                Storage::delete('public/profile_images/' . $user->profile_image);
            }

            // Hapus pengguna dari database
            $user->delete();

            return redirect()->route('user.index')->with(['success' => "User berhasil dihapus"]);
        } else {
            return redirect()->route('user.index')->with(['error' => "User tidak ditemukan"]);
        }
    }


    /**
     * Show the form for editing the user's profile.
     */
    public function editProfile()
    {
        $user = auth()->user();
        return view('pages.akun.editProfile', compact('user'));
    }

    /**
     * Update the user's profile information.
     */
    public function updateProfile(Request $request, string $id)
    {
        $user = User::find($id);

        $request->validate([
            'name' => 'nullable|string|max:255',
            'email' => [
                'nullable',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
            'profile_image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        // Update only the fields that are provided in the request
        $user->fill($request->only(['name', 'email']));

        if ($request->hasFile('profile_image')) {
            $profileImage = $request->file('profile_image');
            $imageName = time() . '.' . $profileImage->extension();
            $profileImage->storeAs('public/profile_images', $imageName);
            $user->profile_image = $imageName;
        }

        $user->save();

        return redirect()->route('user.editProfile')->with(['success' => "Profile Updated successfully!"]);
    }

    public function showLanguageForm()
    {
        $user = Auth::user();
        $bahasas = Bahasa::all();
        return view('pages.setting.bahasa', compact('user', 'bahasas'));
    }

    public function updateLanguage(Request $request)
    {
        // Validasi formulir
        $request->validate([
            'language_id' => 'required|exists:bahasas,id',
        ]);

        // Perbarui kolom language_id untuk pengguna yang sedang masuk
        $user = Auth::user();
        $successfulTransaction = Transaksi::where('email', $user->email)->where('status', 'sukses')->first();

        if ($successfulTransaction) {
            return redirect('/transaksi')->with(['error' => 'Anda tidak dapat mengubah bahasa karena telah menyelesaikan transaksi.']);
        }

        // Update language_id
        $user->language_id = $request->input('language_id');
        $user->save();

        // Redirect or provide response as needed
        return redirect()->route('course.index')->with('success', 'Bahasa berhasil diperbarui');
    }
}
<?php

namespace App\Http\Controllers;

use App\Models\Bahasa;
use App\Models\Materi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MateriController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $searchQuery = $request->input('search');

        $materis = Materi::with('bahasa')
            ->when($searchQuery, function ($query) use ($searchQuery) {
                $query->where('judul', 'like', '%' . $searchQuery . '%');
            })
            ->get();

        return view('admin.materi.index', compact('materis'));
    }

    /**
     * Show the form for creating a new r
     * esource.
     */
    public function create()
    {
        $parents = Bahasa::with('materi')->get();
        return view('admin.materi.create', compact('parents'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul'     => 'required',
            'image'      => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'parent_id' =>  'nullable|exists:bahasas,id',
        ]);

        $image = $request->file('image');
        $image->storeAs('public/materis', $image->hashName());

        Materi::create([
            'judul'    => $request->judul,
            'image'     => $image->hashName(),
            'parent_id' => $request->input('parent_id'),
        ]);

        return redirect()->route('materi.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Materi $materi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Materi $materi)
    {
        $parents = Bahasa::with('materi')->get();
        return view('admin.materi.edit', compact('materi', 'parents'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Materi $materi)
    {
        $this->validate($request, [
            'judul'     => 'required',
            'image'      => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
            'parent_id' =>  'nullable|exists:bahasas,id',
        ]);

        $existingImage = $materi->image;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image->storeAs('public/materis', $image->hashName());

            if ($existingImage) {
                Storage::delete('public/materis/' . $existingImage);
            }

            $materi->update([
                'judul'    => $request->judul,
                'image'     => $image->hashName(),
                'parent_id' => $request->input('parent_id'),
            ]);
        } else {
            $materi->update([
                'judul'    => $request->judul,
                'image'     => $existingImage,
                'parent_id' => $request->input('parent_id')
            ]);
        }

        // if ($request->input('parent_id')) {
        //     $activeParent = Materi::find($request->input('parent_id'));
        //     $activeParent->update(['active' => true]);
        // }

        return redirect()->route('materi.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Materi $materi)
    {
        Storage::delete('public/materis/' . $materi->image);
        $materi->delete();
        return redirect()->route('materi.index')->with(['success' => 'Data Berhasil di Delete']);
    }
}

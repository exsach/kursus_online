<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Penilaian;
use App\Models\Quiz;
use Illuminate\Http\Request;

class PenilaianController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $penilaians = Penilaian::with('user', 'quiz')->paginate(5);
        return view('admin.penilaian.index', compact('penilaians'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $users = User::with('penilaian')->get();
        $quizzes = Quiz::with('penilaian')->get();
        return view('admin.penilaian.create', compact('users', 'quizzes'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id'     => 'required',
            'quiz_id'     => 'required',
            'jawaban'     => 'required',
        ]);

        $quiz = Quiz::findOrFail($request->input('quiz_id'));
        $jawabanBenar = $quiz->jawaban;
        $skorBenar = $quiz->skor;
    
        // Menentukan status berdasarkan perbandingan jawaban
        $status = $request->jawaban == $jawabanBenar ? 'benar' : 'salah';
    
        // Menentukan skor berdasarkan status
        $skor = $status == 'benar' ? $skorBenar : 0;

        Penilaian::create([
            'user_id' => $request->input('user_id'),
            'quiz_id' => $request->input('quiz_id'),
            'jawaban'    => $request->jawaban,
            'status'    => $status,
            'skor'    => $skor,
        ]);

        return redirect()->route('penilaian.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Penilaian $penilaian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Penilaian $penilaian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Penilaian $penilaian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Penilaian $penilaian)
    {
        //
    }
}
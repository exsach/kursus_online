<?php

namespace App\Http\Controllers;

use Midtrans\Snap;
use Midtrans\Config;
use App\Models\Transaksi;
use Midtrans\Transaction;
use Midtrans\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function pay()
    {
        return view('pages.transaksi.index');
    }

    public function __construct()
    {
        \Midtrans\Config::$serverKey    = config('services.midtrans.serverKey');
        \Midtrans\Config::$isProduction = config('services.midtrans.isProduction');
        \Midtrans\Config::$isSanitized  = config('services.midtrans.isSanitized');
        \Midtrans\Config::$is3ds        = config('services.midtrans.is3ds');
    }

    public function createTransaction(Request $request)
    {
        if (Auth::check()) {
            $name = Auth::user()->name;
            $email = Auth::user()->email;

            $transaksiData = [
                'name' => $name,
                'email' => $email,
                'amount' => 100000,
                'status' => 'pending',
            ];

            $transaksi = Transaksi::create($transaksiData);

            Config::$serverKey = env('MIDTRANS_SERVER_KEY');
            Config::$clientKey = env('MIDTRANS_CLIENT_KEY');
            Config::$isProduction = false;

            $itemDetails = [
                [
                    'id' => $transaksi->id,
                    'price' => $transaksi->amount,
                    'quantity' => 1,
                    'name' => 'Upgrade Golingua+',
                ],
            ];

            $customerDetails = [
                'first_name' => $name,
                'email' => $email,
            ];

            $transactionDetails = [
                'order_id' => $transaksi->id,
                'gross_amount' => $transaksi->amount,
            ];

            $callbacks = [
                'finish' => url('/kelas'), // Redirect URL after successful payment
                'unfinish' => url('/transaksi'), // Redirect URL after payment is pending or failed
                'error' => url('/transaksi'), // Redirect URL after payment error
            ];

            $params = [
                'transaction_details' => $transactionDetails,
                'item_details' => $itemDetails,
                'customer_details' => $customerDetails,
                'callbacks' => $callbacks,
            ];

            $previousTransaction = Transaksi::where('email', $email)->where('status', 'sukses')->first();
            if ($previousTransaction) {
                // User has already completed a successful transaction
                return redirect('/kelas')->with(['message' => 'Anda sudah menyelesaikan transaksi sebelumnya.']);
            }

            $snapToken = Snap::getSnapToken($params);
            if ($transaksi) {
                $transaksi->snap_token = $snapToken;
                $transaksi->save();

                return response()->json([
                    'snapToken' => $snapToken,
                    'transaksi_id' => $transaksi->id
                ]);
            } else {
                return response()->json(['error' => 'Transaksi tidak dapat dibuat.'], 500);
            }
        } else {
            return redirect('/login');
        }
    }

    public function callback(Request $request)
    {
        $serverKey = config('services.midtrans.serverKey');
$hashed = hash("sha512", $request->order_id . $request->status_code . $request->gross_amount . $serverKey);
        $transaksi = Transaksi::where('id', $request->order_id)->first();

        if ($hashed == $request->signature_key) {
            if ($request->transaction_status == 'capture' || $request->transaction_status == 'settlement') {
                $transaksi->update(['status' => 'sukses']);
            } elseif ($request->transaction_status == 'expire') {
                $transaksi->update(['status' => 'gagal']);
            }
        }
    }
}
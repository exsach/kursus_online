<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PenjelasanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('penjelasans')->insert([
            [
                'judul' => 'Hangeul',
                'isi' => '"Hangeul" (한글) adalah alfabet yang digunakan untuk menulis bahasa Korea. Alfabet ini dikembangkan pada abad ke-15 oleh Raja Sejong yang Agung dan para sarjana di istana Joseon, dan pertama kali diperkenalkan pada tahun 1443. Hangeul terdiri dari karakter huruf konsonan dan vokal yang membentuk suku kata.',
                'contoh' => '',
                'parent_id' => '3',
                'image' => ''
            ],
            [
                'judul' => 'Vokal tunggal',
                'isi' => '2',
                'contoh' => 'ㅏ - a, ㅑ - ya, ㅓ - eo',
                'parent_id' => '3',
                'image' => ''
            ]
        ]);
    }
}
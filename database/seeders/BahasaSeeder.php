<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class BahasaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('bahasas')->insert([
            [
                'bahasa' => 'KOREA',
                'image' => asset('storage/bahasas/1pArckUSU17dqnBk0Mm3pC3YFCr242IimXMmUotF.png'),
            ],
            [
                'bahasa' => 'ARAB',
                'image' => asset('storage/bahasas/5ZISXx9Vmr4DDJQW6Fu7tQCuRiXteQ4eqonZCvUU.jpg'),
            ]
        ]);
    }
}
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MateriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('materis')->insert([
            [
                'judul' => 'Membaca Hangeul',
                'image' => asset('storage/materis/PnghaZKXwfhV3KBppqKKVBgBOC8KsSXpoJfoX1mx.jpg'),
                'parent_id' => '1'
            ],
            [
                'judul' => 'Menulis Hangeul',
                'image' => asset('storage/materis/fI4bLYy5XUa2bvd7Fc4BOvfYNgQJZdhHZGg0h21u.jpg'),
                'parent_id' => '1'
            ]
        ]);
    }
}
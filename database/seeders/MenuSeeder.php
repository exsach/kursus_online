<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('menus')->insert([
            [
                'label' => 'Dashboard',
                'route' => '/admin/dashboard',
                'image' => public_path('img/dashboard.png'),
                'parent_id' => null
            ],
            [
                'label' => 'Menu',
                'route' => '/admin/menu',
                'image' => public_path('img/menu.png'),
                'parent_id' => null
            ],
            [
                'label' => 'User',
                'route' => '/admin/user',
                'image' => public_path('img/user.png'),
                'parent_id' => null
            ],
            [
                'label' => 'Bahasa',
                'route' => '/admin/bahasa',
                'image' => public_path('img/bahasa.png'),
                'parent_id' => null,
            ],
            [
                'label' => 'Level',
                'route' => '/admin/level',
                'image' => public_path('img/level.png'),
                'parent_id' => null,
            ],
            [
                'label' => 'Materi',
                'route' => '/admin/materi',
                'image' => public_path('img/materi.png'),
                'parent_id' => null,
            ],
            [
                'label' => 'Penilaian',
                'route' => '/admin/penilaian',
                'image' => public_path('img/penilaian.png'),
                'parent_id' => null,
            ],
            [
                'label' => 'Penjelasan',
                'route' => '/admin/penjelasan',
                'image' => public_path('img/penjelasan.png'),
                'parent_id' => null,
            ],
            [
                'label' => 'Quiz',
                'route' => '/admin/quiz',
                'image' => public_path('img/quiz.png'),
                'parent_id' => null,
            ]
        ]);
    }
}
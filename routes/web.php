<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\QuizController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\MateriController;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\PenjelasanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', function () {
    if (auth()->check()) {
        $user = auth()->user();

        if ($user->role === 'admin') {
            return redirect('/admin/dashboard');
        } elseif ($user->role === 'user') {
            if ($user->language_id === null) {
                return redirect('/select-language');
            } else {
                return redirect('/kelas');
            }
        }
    }
    return view('pages/welcome');
})->name('home');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/register', [AuthController::class, 'register'])->name('register');
    Route::post('/register', [AuthController::class, 'registerPost'])->name('register');
    Route::get('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/login', [AuthController::class, 'loginPost'])->name('login');
    Route::get('/verify/{token}', [AuthController::class, 'verify'])->name('verify');
    Route::get('/forgot-password', [AuthController::class, 'forgot']);
    Route::post('/forgot-password', [AuthController::class, 'forgot_password']);
    Route::get('/reset/{token}', [AuthController::class, 'reset']);
    Route::post('/reset/{token}', [AuthController::class, 'post_reset']);
    Route::get('/contact', [ReportController::class, 'report'])->name('report');
    Route::post('/submit-report', [ReportController::class, 'submitReport'])->name('submit-report');
});

Route::group(['middleware' => 'auth'], function () {
    Route::delete('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/select-language', [CourseController::class, 'selectLanguage'])->name('select.language');
    Route::post('/save-language', [CourseController::class, 'saveLanguage'])->name('save.language');
    Route::get('/kelas', [CourseController::class, 'index'])->name('course.index');
    Route::get('/kelas/{id}/level', [LevelController::class, 'showLevels'])->name('materi.levels');
    Route::get('/kelas/{materiId}/{levelId}/pages/', [PenjelasanController::class, 'penjelasan'])->name('level.pages');
    Route::get('/kelas/{materiId}/{levelId}/quiz/', [QuizController::class, 'quiz'])->name('quiz.pages');
    Route::get('/setting/akun', [UserController::class, 'editProfile'])->name('user.editProfile');
    Route::put('/setting/update/{id}', [UserController::class, 'updateProfile'])->name('user.updateProfile');
    Route::get('/setting/reset', [AuthController::class, 'password_reset'])->name('reset_password');
    Route::post('/setting/reset', [AuthController::class, 'pw_post_reset']);
    Route::get('/setting/bahasa', [UserController::class, 'showLanguageForm'])->name('language.form');
    Route::put('/setting/language/update', [UserController::class, 'updateLanguage'])->name('language.update');
    Route::get('/transaksi', [TransaksiController::class, 'pay'])->name('transaksi');
    Route::post('/transaksi', [TransaksiController::class, 'createTransaction'])->name('createTransaction');
    // Route::post('/transaksi/create', [TransaksiController::class, 'createTransaction']);
});

Route::middleware(['auth', 'checkUserRole:admin'])->group(function () {
    Route::get('/admin/dashboard', [AdminController::class, 'dashboard']);
    // Route::get('/user-chart', [UserController::class, 'userRegistrationChart'])->name('user.chart');
    Route::resource('/admin/user', \App\Http\Controllers\UserController::class);
    Route::resource('/admin/menu', \App\Http\Controllers\MenuController::class);
    Route::resource('/admin/bahasa', \App\Http\Controllers\BahasaController::class);
    Route::resource('/admin/materi', \App\Http\Controllers\MateriController::class);
    Route::resource('admin/penilaian', \App\Http\Controllers\PenilaianController::class);
    Route::resource('/admin/penjelasan', \App\Http\Controllers\PenjelasanController::class);
    Route::resource('/admin/level', \App\Http\Controllers\LevelController::class);
    Route::resource('/admin/quiz', \App\Http\Controllers\QuizController::class);
    Route::resource('/admin/transaksi', \App\Http\Controllers\TransaksiController::class);
    // Route::get('/admin/dashboard/bahasa', [BahasaController::class, 'index'])->name('admin.bahasa.index');
});